<div class="flex items-center justify-center min-h-screen filament-login-page">
    <div class="p-2 max-w-lg space-y-8 w-screen">
        <form wire:submit.prevent="authenticate" @class([
            'bg-white space-y-8 shadow border border-gray-300 rounded-2xl p-8',
            'dark:bg-gray-800 dark:border-gray-700'
        ])>
        <div class="w-full flex justify-center">
            <x-filament::brand/>
            <img style="height: 50px" src="{{asset('images/nvsu_it2.png')}}" alt="">

        </div>

        <h2 class="font-bold tracking-tight text-center text-lg">
            Information Technology Online Grading System<br>
            Bayombong , Nueva Vizcaya
        </h2>
        @if(request()->has('login_as'))
            {{ $this->form }}
            <x-filament::button type="submit" form="authenticate" class="w-full">
                {{ __('filament::login.buttons.submit.label') }}
            </x-filament::button>
        @else
            <div x-data="{showAboutInfo:false}" >
                <div x-show="showAboutInfo" class="flex flex-col gap-3 hidden" >
                    <button style="background: #4C71E3; border-radius: 20px; display: block; text-align: center; padding: 10px 5px;font-weight: bold;color: white;margin-top: 20px;" type="button" @click="showAboutInfo = false">Back</button>
                    <h2 style="font-size: 30px">ABOUT</h2>
                    <p>Information Technology Online Grading System is a web-based application that allows a professors/instructors input grades and allow students to view their grades online.</p>
                    <h3 style="font-size: 20px;font-weight: bold;text-align: center">ABOUT THE MEMBER</h3>
                    <div class="flex">
                        <div class="flex-1" style="padding-right: 40px;padding-bottom: 30px;min-width: 60%">
                            <strong>ETHEL MAE UBAS AGGALUT</strong>
                            <p>Address: Brgy. Tuao North, Bagabag, Nueva Vizcaya</p>
                            <p>E-Mail: aggalutethelmae13@gmail.com</p>
                            <p>Career Objectives:</p>
                            <p>Aims to learn as much as possible in order to improve my skill set and enhance my understanding of current and emerging IT trends.</p>
                        </div>
                        <div class="flex-1">
                            <img src="{{asset('images/ETHEL MAE UBAS AGGALUT.jpg')}}" alt="">
                        </div>
                    </div>
                    <hr>
                    <div class="flex">
                        <div class="flex-1" style="padding-right: 40px;padding-bottom: 30px;min-width: 60%">
                            <strong>LOVELY MARION BACSA BUTAC</strong>
                            <p>Address: Bonfal Proper,Bayombong,Nueva Vizcaya</p>
                            <p>E-Mail: lovelybutac@gmail.com</p>
                            <p>Career Objectives:</p>
                            <p>Self-motivated, highly passionate and hardworking fresher looking for an opportunity to work in a challenging organization to utilize e my skills and knowledge to work for the growth of the organization.</p>
                        </div>
                        <div class="flex-1">
                            <img style="height: 156px" src="{{asset('images/LOVELY MARION BACSA BUTAC.jpg')}}" alt="">
                        </div>
                    </div>

                    <hr>
                    <div class="flex">
                        <div class="flex-1" style="padding-right: 40px;padding-bottom: 30px;min-width: 60%">
                            <strong>BARBY MORADOS BARCELO</strong>
                            <p>Address: Abuyo, Alfonso Castañeda, Nueva Vizcaya</p>
                            <p>E-Mail: barbybarcelo@gmail.com </p>
                            <p>Career Objectives:</p>
                            <p>I am a determined, very controlled, positive, and focused person, with a resilient work principle to team up in team-based or individually motivated situations. </p>
                        </div>
                        <div class="flex-1">
                            <img src="{{asset('images/BARBY MORADOS BARCELO.jpg')}}" alt="">
                        </div>
                    </div>
                    <hr>
                    <div class="flex">
                        <div class="flex-1" style="padding-right: 40px;padding-bottom: 30px;min-width: 60%">
                            <strong>MICHAELA  JOY MORALES GALAGATA</strong>
                            <p>Address: Purok 7, Magsaysay, Bayombong, Nueva Vizcaya</p>
                            <p>E-Mail: michgalagata@gmail.com</p>
                            <p>Career Objectives:</p>
                            <p>I have the passion and perseverance to make things work. I am optimistic and positive person. A highly organized and hard-working individual that welcomes innovative ideas for accomplishing projects.</p>
                        </div>
                        <div class="flex-1">
                            <img src="{{asset('images/MICHAELA  JOY MORALES GALAGATA.jpg')}}" alt="">
                        </div>
                    </div>
                    <hr>
                    <h3 style="text-align: center;font-weight: bold;font-size: 1.5em">FAQs</h3>
                    <strong>What is Online Grade Viewing?</strong>
                    <p>An online grading system is a computerized where you could only access through website (nvsutitonlinegrading.info) via internet. Professors/teachers or faculty allows to manage and access list of students for each class that they are teaching and perform standard school management such as to submit final grades, incomplete, and failed information. This information is stored in the database MySQL. This information becomes part of students’ academic records and transcripts.</p>
                    <h4 style="font-weight: bold;font-size: 1em">MISSION</h4>
                    <p>To produce independent and self-oriented learned with strong sense of ancestral pride, to help preserve the cultural heritage and lay down the foundation for lifelong learning. </p>
                    <h4 style="font-weight: bold;font-size: 1em">VISION </h4>
                    <p>A school with highly performing and work-oriented students and graduates concern for other.</p>
                </div>
                <div x-show="!showAboutInfo" class="flex flex-col ">
                    <a style="background: #CB8A05; border-radius: 20px; display: block; text-align: center; padding: 10px 5px;font-weight: bold;color: white;margin-top: 20px;" href="/admin/login?login_as=Administrator">Administrator</a>
                    <a style="background: #CB8A05; border-radius: 20px; display: block; text-align: center; padding: 10px 5px;font-weight: bold;color: white;margin-top: 20px;" href="/admin/login?login_as=Instructor">Instructor</a>
                    <a style="background: #CB8A05; border-radius: 20px; display: block; text-align: center; padding: 10px 5px;font-weight: bold;color: white;margin-top: 20px;" class="" href="/admin/login?login_as=Student">Student</a>
                    <button style="background: #CB8A05; border-radius: 20px; display: block; text-align: center; padding: 10px 5px;font-weight: bold;color: white;margin-top: 20px;" @click="showAboutInfo = true" >About</button>
                </div>

            </div>
        @endif
        </form>
            <x-filament::footer/>
    </div>
</div>
