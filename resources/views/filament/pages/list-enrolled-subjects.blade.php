<x-filament::page>
    <h2 class="text-2xl font-bold">Adviser :  {{$adviser}}</h2>
    @livewire('list-enrolled-subjects',['student'=>auth()->user()])
</x-filament::page>
