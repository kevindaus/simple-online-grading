<x-filament::page>
    <style>
        #student-grade-breakdown dl > div {
            padding: 20px 20px;
            display: flex;
            gap: 10;
        }
        #student-grade-breakdown dl > div > * {
            width: 250px;
            color: black;
        }
    </style>
    @php
        $alternateIndex = 1;
    @endphp
    <div class="relative p-6 rounded-2xl bg-white shadow">
        <div class="space-y-2">
            @if($gradeDescription)
            <p class="mt-1 max-w-2xl text-2xl text-gray-500" style="padding-bottom: 10px">
                {{$gradeDescription}}
            </p>
            @endif
            <div class="bg-white max-w-2xl shadow overflow-hidden sm:rounded-lg">

                <div class="px-4 py-5 sm:px-6">
                    <h3 class="text-lg leading-6 font-medium text-gray-900" style="padding: 20px 20px 20px 0px">
                        Grade : {{ $final_grade }}
                    </h3>
                </div>
                <div class="border-t border-gray-200" id="student-grade-breakdown">
                    <dl>
                        @foreach($grade->prelim_quizzes as $currentQuizKey => $currentQuizValue)
                            <div @class([
                                    'px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6',
                                ])>
                                <dt class="text-sm font-medium text-gray-500 p-5">
                                    Prelim {{$currentQuizKey}}
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{$currentQuizValue}}
                                </dd>
                            </div>
                        @endforeach
                        <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                            <dt class="text-sm font-medium text-gray-500">
                                Prelim Requirement
                            </dt>
                            <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                {{$grade->prelim_requirement}}
                            </dd>
                        </div>

                        @foreach($grade->prelim_exams as $currentQuizKey => $currentQuizValue)
                            <div @class([
                                'px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6',
                            ])>
                                <dt class="text-sm font-medium text-gray-500 p-5">
                                    Prelim {{$currentQuizKey}}
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{$currentQuizValue}}
                                </dd>
                            </div>
                        @endforeach

                        @foreach($grade->midterm_quizzes as $currentQuizKey => $currentQuizValue)
                            <div @class([
                                'px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6',
                            ])>
                                <dt class="text-sm font-medium text-gray-500 p-5">
                                    Midterm {{$currentQuizKey}}
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{$currentQuizValue}}
                                </dd>
                            </div>
                        @endforeach
                        @foreach($grade->midterm_exams as $currentQuizKey => $currentQuizValue)
                            <div @class([
                                'px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6',
                            ])>
                                <dt class="text-sm font-medium text-gray-500 p-5">
                                    Midterm {{$currentQuizKey}}
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{$currentQuizValue}}
                                </dd>
                            </div>
                        @endforeach
                        <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                            <dt class="text-sm font-medium text-gray-500">
                                Midterm Requirement
                            </dt>
                            <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                {{$grade->midterm_requirement}}
                            </dd>
                        </div>

                        @foreach($grade->finals_quizzes as $currentQuizKey => $currentQuizValue)
                            <div @class([
                            'px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6',
                        ])>
                                <dt class="text-sm font-medium text-gray-500 p-5">
                                    Finals {{$currentQuizKey}}
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{$currentQuizValue}}
                                </dd>
                            </div>
                        @endforeach
                        @foreach($grade->finals_exams as $currentQuizKey => $currentQuizValue)
                            <div @class([
                            'px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6',
                        ])>
                                <dt class="text-sm font-medium text-gray-500 p-5">
                                    Finals {{$currentQuizKey}}
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{$currentQuizValue}}
                                </dd>
                            </div>
                        @endforeach
                        <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                            <dt class="text-sm font-medium text-gray-500">
                                Final Requirement
                            </dt>
                            <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                {{$grade->final_requirement}}
                            </dd>
                        </div>
                    </dl>
                </div>
            </div>

        </div>

    </div>


</x-filament::page>
