@extends('vendor.filament.components.')

@section('content')
    <h1 class="text-3xl">hello world</h1>
    <form wire:submit.prevent="authenticate" class="bg-white space-y-8 shadow border border-gray-300 rounded-2xl p-8">
        <div class="w-full flex justify-center">
            <img src="http://127.0.0.1:8000/images/nvsu.jpg" >
        </div>

        <h2 class="font-bold tracking-tight text-center text-2xl">
            Sign in to your account
        </h2>

        <div class="grid gap-6 filament-forms-component-container grid-cols-1">
            <div class="col-span-1 ">
                <div class="filament-forms-field-wrapper">

                    <div class="space-y-2">

                        <div class="filament-forms-placeholder-component">
                            <h3>Admin</h3>
                        </div>


                    </div>
                </div>

            </div>
            <div class="col-span-1 ">
                <div class="filament-forms-field-wrapper">

                    <div class="space-y-2">
                        <div class="flex items-center justify-between space-x-2 rtl:space-x-reverse">
                            <label class="inline-flex items-center space-x-3 rtl:space-x-reverse filament-forms-field-wrapper-label" for="identification_number">


    <span class="text-sm font-medium leading-4 text-gray-700">
        ID #

                    <sup class="font-medium text-danger-700">*</sup>
            </span>


                            </label>

                        </div>

                        <div class="flex items-center space-x-1 group filament-forms-text-input-component">

                            <div class="flex-1">
                                <input
                                    wire:model.defer="identification_number"
                                    type="text"

                                    autocomplete="on"


                                    id="identification_number"








                                    required
                                    class="block w-full transition duration-75 rounded-lg shadow-sm focus:border-primary-600 focus:ring-1 focus:ring-inset focus:ring-primary-600 disabled:opacity-70 border-gray-300"
                                />
                            </div>

                        </div>


                    </div>
                </div>

            </div>
            <div class="col-span-1 ">
                <div class="filament-forms-field-wrapper">

                    <div class="space-y-2">
                        <div class="flex items-center justify-between space-x-2 rtl:space-x-reverse">
                            <label class="inline-flex items-center space-x-3 rtl:space-x-reverse filament-forms-field-wrapper-label" for="password">


    <span class="text-sm font-medium leading-4 text-gray-700">
        Password

                    <sup class="font-medium text-danger-700">*</sup>
            </span>


                            </label>

                        </div>

                        <div class="flex items-center space-x-1 group filament-forms-text-input-component">

                            <div class="flex-1">
                                <input
                                    wire:model.defer="password"
                                    type="password"




                                    id="password"








                                    required
                                    class="block w-full transition duration-75 rounded-lg shadow-sm focus:border-primary-600 focus:ring-1 focus:ring-inset focus:ring-primary-600 disabled:opacity-70 border-gray-300"
                                />
                            </div>

                        </div>


                    </div>
                </div>

            </div>
            <div class="col-span-1 ">
                <div class="filament-forms-field-wrapper">

                    <div class="space-y-2">
                        <div class="flex items-center justify-between space-x-2 rtl:space-x-reverse">
                            <label class="inline-flex items-center space-x-3 rtl:space-x-reverse filament-forms-field-wrapper-label" for="remember">
                                <input


                                    id="remember"
                                    type="checkbox"

                                    wire:model.defer="remember"
                                    class="text-primary-600 transition duration-75 rounded shadow-sm focus:border-primary-500 focus:ring-2 focus:ring-primary-500 filament-forms-checkbox-component border-gray-300"
                                />

                                <span class="text-sm font-medium leading-4 text-gray-700">
        Remember me

            </span>


                            </label>

                        </div>




                    </div>
                </div>

            </div>
        </div>


        <button
            type="submit"
            wire:loading.attr="disabled"
            class="inline-flex items-center justify-center font-medium tracking-tight rounded-lg border transition-colors focus:outline-none focus:ring-offset-2 focus:ring-2 focus:ring-inset filament-button h-9 px-4 text-white shadow focus:ring-white border-transparent bg-primary-600 hover:bg-primary-500 focus:bg-primary-700 focus:ring-offset-primary-700 w-full"
        >
            <svg
                wire:loading
                wire:target=authenticate
                class="filament-button-icon w-6 h-6 mr-1 -ml-2 rtl:ml-1 rtl:-mr-2 animate-spin"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                fill="currentColor"
            >
                <path d="M2 12C2 6.47715 6.47715 2 12 2V5C8.13401 5 5 8.13401 5 12H2Z" />
            </svg>

            <span>Sign in</span>

        </button>
    </form>

@endsection
