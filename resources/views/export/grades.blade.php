<style>
    table tr td {
        border: 1px solid black;
        padding: 10px;
    }
</style>
<div style="text-align: center">
    <img src="data:image/png;base64, {{ $nvsu_logo  }}" style="height: 130px;float: left">
    <img src="data:image/png;base64, {{ $nvsu_it_logo  }}" style="height: 130px; float: right" >
    <strong style="font-size: 1.5em">Nueva Vizcaya State University</strong>
    <br>
    <strong>Bayombong , Nueva Vizcaya</strong>
    <br>
    <br>
    <strong style="font-size: 1.2em">Enrolled Subjects</strong>
    <br>
    <strong>Officially Enrolled</strong>
</div>
<br>
<br>
<div >
    <div style="text-align: left">
        <strong>ID #:   {{ $idNumber  }}</strong> <br>
        <strong>Student : {{ $studentName }}</strong>
    </div>
    <div style="text-align: right;position: relative;top: -20px">
        <strong>{{$courseAndYear}}</strong>
    </div>
</div>
<br>
<table class="equalDivide" cellpadding="0" cellspacing="0" width="100%" border="0">
    <thead>
    <tr>
        <td><strong>Code</strong></td>
        <td><strong>Name</strong></td>
        <td><strong>Grade</strong></td>
        <td><strong>Remarks</strong></td>
    </tr>
    </thead>
    <tbody>

    @foreach($grades as $semester => $currentGrade)
        <tr>
            <td></td>
            <td style="font-size: 1em;font-weight: bold;text-transform: uppercase">{{  \App\Models\User::getYearLevelLabel()[auth()->user()->year_level] }} : {{ $semester  }} </td>
            <td></td>
            <td></td>
        </tr>
        @foreach($currentGrade as $grade)
            @php
                $gradeCalculator = new App\Service\GradeCalculator($grade);
                $finalGrade = $gradeCalculator->gradeViaPointSystem();
                $remarks = ($finalGrade <= 3) ? "Passed":"Failed";
            @endphp
            <tr>
                <td>{{$grade->subject->name}}</td>
                <td>{{$grade->subject->description}}</td>
                <td>{{$finalGrade}}</td>
                <td>{{  $remarks  }}</td>
            </tr>
        @endforeach
    @endforeach
    </tbody>
</table>

