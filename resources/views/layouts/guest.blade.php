<!DOCTYPE html>
<html
    lang="{{ str_replace('_', '-', app()->getLocale()) }}"
    dir="{{ __('filament::layout.direction') ?? 'ltr' }}"
    class="filament antialiased bg-gray-100 js-focus-visible"
>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') {{ config('app.name') }}</title>
    <title>Login -  Information Technology Online Grading System</title>

    <style>
        [x-cloak=""], [x-cloak="1"] { display: none !important; }
        @media (max-width: 1023px) { [x-cloak="-lg"] { display: none !important; } }
        @media (min-width: 1024px) { [x-cloak="lg"] { display: none !important; } }
    </style>

    <!-- Livewire Styles -->
    <style >
        [wire\:loading], [wire\:loading\.delay], [wire\:loading\.inline-block], [wire\:loading\.inline], [wire\:loading\.block], [wire\:loading\.flex], [wire\:loading\.table], [wire\:loading\.grid], [wire\:loading\.inline-flex] {
            display: none;
        }

        [wire\:loading\.delay\.shortest], [wire\:loading\.delay\.shorter], [wire\:loading\.delay\.short], [wire\:loading\.delay\.long], [wire\:loading\.delay\.longer], [wire\:loading\.delay\.longest] {
            display:none;
        }

        [wire\:offline] {
            display: none;
        }

        [wire\:dirty]:not(textarea):not(input):not(select) {
            display: none;
        }

        input:-webkit-autofill, select:-webkit-autofill, textarea:-webkit-autofill {
            animation-duration: 50000s;
            animation-name: livewireautofill;
        }

        @keyframes livewireautofill { from {} }
    </style>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400;1,500;1,700&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    @livewireStyles

</head>

<body class="bg-gray-100 text-gray-900 filament-body">

<div wire:id="YmtmRGQ64lLTDPhVLdrk" wire:initial-data="{&quot;fingerprint&quot;:{&quot;id&quot;:&quot;YmtmRGQ64lLTDPhVLdrk&quot;,&quot;name&quot;:&quot;filament.pages.custom-login&quot;,&quot;locale&quot;:&quot;en&quot;,&quot;path&quot;:&quot;admin\/login&quot;,&quot;method&quot;:&quot;GET&quot;,&quot;v&quot;:&quot;acj&quot;},&quot;effects&quot;:{&quot;listeners&quot;:[]},&quot;serverMemo&quot;:{&quot;children&quot;:[],&quot;errors&quot;:[],&quot;htmlHash&quot;:&quot;385dd54b&quot;,&quot;data&quot;:{&quot;email&quot;:&quot;&quot;,&quot;password&quot;:null,&quot;remember&quot;:false,&quot;componentFileAttachments&quot;:[],&quot;identification_number&quot;:null},&quot;dataMeta&quot;:[],&quot;checksum&quot;:&quot;e8683cb2b3d61a6ed215f8add11737a588f00ade420bd9342e0d21b33502f35b&quot;}}" class="flex items-center justify-center min-h-screen filament-login-page">
    <div class="p-2 max-w-md space-y-8 w-screen">
        @yield('content')

    </div>
</div>

<!-- Livewire Scripts -->
@livewireScripts
<script src="{{asset('js/app.js')}}"></script>


</body>
</html>
