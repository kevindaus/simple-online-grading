<div class="p-5">
    <div class="block w-full overflow-x-auto">
        <table class="items-center bg-transparent w-full border-collapse ">
            <thead>
            <tr>
                <th class="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-lg uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                    Year
                </th>
                <th class="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-lg uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                    User
                </th>
                <th class="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-lg uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                    Subject
                </th>
                <th class="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-lg uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                    Grade
                </th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <th class="border-t-0 px-6 align-middle border-l-0 border-r-0  whitespace-nowrap p-4 text-left text-blueGray-700">

                    @if($year_level !== "4")
                        {{ \App\Models\User::getYearLevelLabel()[$year_level] }}
                    @else
                        4th year NDM <br>
                        4th year WMAD
                    @endif
                </th>
                <td class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-lg whitespace-nowrap p-4 ">
                    {{ $student_count }}

                </td>
                <td class="border-t-0 px-6 align-center border-l-0 border-r-0 text-lg whitespace-nowrap p-4">
                    {{$subject_count}}
                </td>
                <td class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-lg whitespace-nowrap p-4">
                    {{$grade_count}}
                </td>
            </tr>
            </tbody>

        </table>
    </div>
</div>
