<?php

namespace Tests\Feature;

use App\Models\Grade;
use App\Models\Subject;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class GradeTest extends TestCase
{
    use DatabaseMigrations;

    public function testHasManyThrough(): void
    {
        $user = User::factory()->createOne();
        $subject = Subject::factory()->createOne();
        foreach (range(0, 20) as $cur) {
            $grade = Grade::factory()->createOne([
                'user_id' => $user->id,
                'subject_id' => $subject->id,
            ]);
        }
        self::assertNotEquals(0, $subject->enrollees()->count());

    }
}
