<?php

namespace Tests\Feature;

use App\Filament\Resources\GradeResource;
use App\Filament\Resources\UserResource;
use App\Models\Grade;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class TeacherFeatureTest extends TestCase
{
    use DatabaseMigrations;
    public function testTeacherFeatures(): void
    {
        /* @var $teacher User */
        $teacher = User::factory()->create();
        $teacherRole = Role::findOrCreate(User::TEACHER_ROLE);
        $teacher->syncRoles($teacherRole);
        $student = User::factory()->create();
        $studentRole = Role::findOrCreate(User::STUDENT_ROLE);
        $student->syncRoles($studentRole);
        $this->actingAs($teacher);

        /*students*/
        $this->get("/admin/my-students")->assertSuccessful();
        /*edit a student*/
        $this->get(UserResource::getUrl('edit', ['record' => $student ]))->assertSuccessful();


        /*grades*/
        $this->get(GradeResource::getUrl('index'))->assertSuccessful();
        $this->get(GradeResource::getUrl('create'))->assertSuccessful();
        $testGrade = Grade::factory()->create([
            'user_id' =>$student->id
        ]);
        $this->get(GradeResource::getUrl('edit',['record'=>$testGrade]))->assertSuccessful();


    }
}
