<?php

namespace Tests\Feature;

use App\Filament\Resources\GradeResource;
use App\Models\Grade;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class StudentFeatureTest extends TestCase
{
    use DatabaseMigrations;
    public function testStudentFeatures(): void
    {
        /* @var $student User */
        $student = User::factory()->create();
        $studentRole = Role::findOrCreate(User::STUDENT_ROLE);
        $student->syncRoles($studentRole);
        $this->actingAs($student);

        $this->get("/admin/my-grade");
        $grade = Grade::factory()->create([
            'user_id' => $student->id
        ]);
        $this->get(GradeResource::getUrl('show',['record'=> $grade]))->assertSuccessful();

    }
}
