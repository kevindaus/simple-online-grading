<?php

namespace Tests\Feature;

use App\Filament\Resources\GradeResource;
use App\Filament\Resources\SubjectResource;
use App\Filament\Resources\UserResource;
use App\Models\Grade;
use App\Models\Subject;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class AdminFeatureTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function testAdminFeatures(): void
    {
        /* @var $admin User */
        $admin = User::factory()->create();
        $adminRole = Role::findOrCreate(User::ADMINISTRATOR_ROLE);
        $admin->syncRoles($adminRole);

        $this->get('/admin/login')->assertSuccessful();

        $this->actingAs($admin);
        /*user*/

        $this->get(UserResource::getUrl('create'))->assertSuccessful();
        $createdUser = User::factory()->create();
        $createdUser->syncRoles($adminRole);
        $studentUser = User::factory()->create();
        $studentRole = Role::findOrCreate(User::STUDENT_ROLE);
        $teacherRole = Role::findOrCreate(User::TEACHER_ROLE);
        $studentUser->syncRoles($studentRole);

        $this->get(UserResource::getUrl('edit',['record'=>$studentUser]))->assertSuccessful();

        /*subjects*/
        $this->get(SubjectResource::getUrl('index'))->assertSuccessful();
        $this->get(SubjectResource::getUrl('create'))->assertSuccessful();
        $newSubject = Subject::factory()->create();
        $this->get(SubjectResource::getUrl('edit',['record'=>$newSubject]))->assertSuccessful();

        /*grades*/
        $this->get(GradeResource::getUrl('index'))->assertSuccessful();

    }

    public function testAdminCanViewNewUser(): void
    {
        /* @var $admin User */
        $admin = User::factory()->create();
        $adminRole = Role::findOrCreate(User::ADMINISTRATOR_ROLE);
        $admin->syncRoles($adminRole);

        $this->get('/admin/login')->assertSuccessful();

        $this->actingAs($admin);

        $this->get("/admin/users/{$admin->id}/edit")->assertSuccessful();

    }


}
