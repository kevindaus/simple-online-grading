<?php

namespace Tests\Feature;

use App\Models\Grade;
use App\Service\GradeCalculator;
use App\Settings\PerfectScoreSettings;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class GradeCalculatorTest extends TestCase
{
    use DatabaseMigrations;
    public function testGradeCalculator(): void
    {
        PerfectScoreSettings::fake([
                "prelim_quizzes"=> [
                    "Quiz 1"=>  50,
                    "Quiz 2"=>  30,
                    "Quiz 3"=>  25,
                ],
                "prelim_exams"=>  [
                    'Exam 1'=> 100
                ],
                "midterm_quizzes"=> [
                    "Quiz 1"=>  50,
                    "Quiz 2"=>  50,
                    "Quiz 3"=>  50,
                ] ,
                "midterm_exams"=>  [
                    "Exam 1"=>  100,
                ],
                "finals_quizzes"=>  [
                    "Quiz 1"=>  20,
                    "Quiz 2"=>  100,
                    "Quiz 3"=>  100,
                ],
                "finals_exams"=>  [
                    "Exam 1"=>  100,
                ],
                "prelim_requirement"=>  100,
                "midterm_requirement"=>  100,
                "final_requirement"=>  100,
        ]);
        $grade = Grade::factory()->create([
            "prelim_quizzes"=> [
                "Quiz 1"=>  50,
                "Quiz 2"=>  30,
                "Quiz 3"=>  25,
            ],
            "prelim_exams"=>  [
                'Exam 1'=> 100
            ],
            "midterm_quizzes"=> [
                "Quiz 1"=>  50,
                "Quiz 2"=>  50,
                "Quiz 3"=>  50,
            ] ,
            "midterm_exams"=>  [
                "Exam 1"=>  100,
            ],
            "finals_quizzes"=>  [
                "Quiz 1"=>  20,
                "Quiz 2"=>  100,
                "Quiz 3"=>  100,
            ],
            "finals_exams"=>  [
                "Exam 1"=>  100,
            ],
            "prelim_requirement"=>  100,
            "midterm_requirement"=>  100,
            "final_requirement"=>  100,
        ]);
        $gradeCalculator = new GradeCalculator($grade);
        $finalGrade = $gradeCalculator->getFinalGrade();
        self::assertEquals(1.25,$gradeCalculator->gradeViaPointSystem());
        self::assertGreaterThan(90 , $gradeCalculator->getFinalGrade());

    }
}
