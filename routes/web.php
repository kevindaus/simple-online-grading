<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect("/admin");
});

Route::get('/pre-login', function () {
    return redirect("/admin");
})->name('pre-login');


Route::get('/login', function () {
    return redirect("/admin");
})->name('login');

Route::group(['middleware' => ['auth','role:'.\App\Models\User::STUDENT_ROLE ]], function () {
    Route::get('export', [\App\Http\Controllers\GradeExporterController::class, 'index'])->name('grade.export');
});
