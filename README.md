

#Online Grading System using Laravel T.A.L.L Stack with Filament
This is a simple demo project that showcases the use of T.A.L.L Stack with Filament.

#### Features
* Student , Instructor , Subject and Grade Management
* Grade Report
* Score Configuration Settings


### Requirement
<ul>
<li>MySQL 5.7.24</li>
<li>PHP 8.0 or higher</li>
<li>Composer version 2.0.8</li>
<li>NPM version 7.20.3</li>
</ul>

### Installation
<ol>
<li>git clone https://gitlab.com/kevindaus/simple-online-grading.git</li>
<li>cd simple-online-grading</li>
<li>composer install</li>
<li>npm run demo-mode</li>
</ol>

### Screenshots

![Alt text](screenshots/screenshot1.png?raw=true)
![Alt text](screenshots/screenshot2.jpg?raw=true)
![Alt text](screenshots/screenshot3.jpg?raw=true)
![Alt text](screenshots/screenshot4.jpg?raw=true)

