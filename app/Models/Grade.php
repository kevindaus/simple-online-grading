<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    use HasFactory;

    const STATUS_OFFICIALLY_DROPPED = 'OD';
    const STATUS_UNOFFICIALLY_DROPPED = 'UD';
    const STATUS_INCOMPLETE = 'INC';
    const STATUS_COMPLETE = 'COMPLETE';

    const FIRST_SEMESTER = 'first_semester';
    const SECOND_SEMESTER = 'second_semester';

    protected $fillable = [
        'semester',
        'year_level',
        'user_id',
        'subject_id',

        'prelim_quizzes',
        'prelim_exams',
        'midterm_quizzes',
        'midterm_exams',
        'finals_quizzes',
        'finals_exams',

        'prelim_quiz_1',
        'prelim_quiz_2',
        'prelim_quiz_3',
        'prelim_exam',
        'prelim_requirement',
        'midterm_quiz_1',
        'midterm_quiz_2',
        'midterm_quiz_3',
        'midterm_requirement',
        'midterm_exam',
        'final_quiz_1',
        'final_quiz_2',
        'final_quiz_3',
        'final_requirement',
        'final_exam',
        'status',
    ];
    protected $casts = [
        'prelim_quizzes'=>'json',
        'prelim_exams'=>'json',
        'midterm_quizzes'=>'json',
        'midterm_exams'=>'json',
        'finals_quizzes'=>'json',
        'finals_exams'=>'json',

        'prelim_quiz_1'=>'float',
        'prelim_quiz_2'=>'float',
        'prelim_quiz_3'=>'float',
        'prelim_exam'=>'float',
        'prelim_requirement'=>'float',
        'midterm_quiz_1'=>'float',
        'midterm_quiz_2'=>'float',
        'midterm_quiz_3'=>'float',
        'midterm_exam'=>'float',
        'midterm_requirement'=>'float',
        'final_quiz_1'=>'float',
        'final_quiz_2'=>'float',
        'final_quiz_3'=>'float',
        'final_exam'=>'float',
        'final_requirement'=>'float',
        'created_at'=>'datetime',
        'updated_at'=>'datetime',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public static function getSemesterLabel()
    {
        return [
            ''=>'',
            Grade::FIRST_SEMESTER => '1st sem',
            Grade::SECOND_SEMESTER => '2nd sem',
        ];
    }


}
