<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use HasFactory;

    const FIRST_SEMESTER = 'first_semester';
    const SECOND_SEMESTER = 'second_semester';

    protected $fillable = [
        'name',
        'description',
        'year_level',
        'semester',
    ];

    public function grades()
    {
        return $this->hasMany(Grade::class);
    }

    public function enrollees()
    {
        return $this->hasManyThrough(
            User::class,
            Grade::class,
            'subject_id',
            'id'
        );
    }

}
