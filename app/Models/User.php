<?php

namespace App\Models;

use Filament\Models\Contracts\FilamentUser;
use Filament\Models\Contracts\HasName;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable implements FilamentUser , HasName
{
    use HasApiTokens, HasFactory, Notifiable;
    use HasRoles;
    use HasFactory;

    const TEACHER_ROLE = 'teacher';
    const STUDENT_ROLE = 'student';
    const ADMINISTRATOR_ROLE = 'admin';

    const FIRST_YEAR_LEVEL = '1';
    const SECOND_YEAR_LEVEL = '2';
    const THIRD_YEAR_LEVEL = '3';
    const FOURTH_YEAR_LEVEL = '4';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'identification_number',
        'name',
        'mobile_number',
        'year_level',
        'course',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function canAccessFilament(): bool
    {
        return $this->hasAnyRole([self::ADMINISTRATOR_ROLE, self::TEACHER_ROLE, self::STUDENT_ROLE]);
    }


    public function getFilamentName(): string
    {
        return $this->name;
    }

    public function grades()
    {
        return $this->hasMany(Grade::class);
    }

    public static function getYearLevelLabel()
    {
        return [
            '' => '--',
            static::FIRST_YEAR_LEVEL => 'first year',
            static::SECOND_YEAR_LEVEL => 'second year',
            static::THIRD_YEAR_LEVEL => 'third year',
            static::FOURTH_YEAR_LEVEL => 'fourth year',
        ];
    }

    public function subjects()
    {
        return $this->hasManyThrough(
            Subject::class,
            Grade::class,
            'user_id',
            'id'
        );
    }
}
