<?php

namespace App\Providers;

use App\Service\GradeCalculator;
use Illuminate\Support\ServiceProvider;

class OnlineGradingSystemServiceProvider extends ServiceProvider
{
    public function register()
    {

    }

    public function boot()
    {
        //
    }
}
