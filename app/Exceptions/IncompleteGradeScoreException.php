<?php

namespace App\Exceptions;

use Exception;

class IncompleteGradeScoreException extends Exception
{
}
