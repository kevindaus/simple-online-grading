<?php

namespace App\Filament\Widgets;

use App\Models\User;
use Filament\Widgets\Widget;

class StudentSubjectWidget extends Widget
{
    protected int|string|array $columnSpan = 'full';
    protected static string $view = 'filament.widgets.student-subject-widget';
    public static function canView(): bool
    {
        return auth()->user()->hasRole(User::STUDENT_ROLE);
    }
}
