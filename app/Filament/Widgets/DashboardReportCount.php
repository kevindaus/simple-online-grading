<?php

namespace App\Filament\Widgets;

use App\Models\Grade;
use App\Models\Subject;
use App\Models\User;
use Filament\Widgets\Widget;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Card;

class DashboardReportCount extends BaseWidget
{

    protected function getCards(): array
    {
        return [
            Card::make('User', User::role([User::STUDENT_ROLE])->count())->description('Number of students'),
            Card::make('Subject', Subject::count())->description('Number of subjects'),
            Card::make('Grades', Grade::count())->description('Number of grades'),
            Card::make('User', User::role([User::TEACHER_ROLE])->count())->description('Faculty Teacher'),
        ];
    }
    public static function canView(): bool
    {
        return auth()->user()->hasAnyRole([User::ADMINISTRATOR_ROLE ]);
    }
}
