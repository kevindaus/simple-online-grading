<?php

namespace App\Filament\Widgets;

use App\Exceptions\IncompleteGradeScoreException;
use App\Models\Grade;
use App\Models\User;
use App\Service\GradeCalculator;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;
use Filament\Widgets\StatsOverviewWidget\Card;

use phpDocumentor\Reflection\DocBlock\Tags\Author;

class ShowGradesOverview extends BaseWidget
{
//    protected static string $view = 'filament.widgets.show-grades-overview';


    protected function getCards(): array
    {
        /*get all grades by current student */
        /* @var $currentStudent User */
        $currentStudent = auth()->user();
        $cardCollection = [];
        $currentStudent->grades()->each(function(Grade $currentGrade) use (&$cardCollection) {
            $subject = sprintf("%s (%s)",$currentGrade->subject->name, Grade::getSemesterLabel()[$currentGrade->semester] );
            $gradeCalculator = new GradeCalculator($currentGrade);
            $finalGrade = '--';
            try {
                $finalGrade = $gradeCalculator->gradeViaPointSystem();
            } catch (IncompleteGradeScoreException $ex) {
                $finalGrade = "--";
            }
            $breakdownDescription = sprintf(
                "Computed Prelim : %s, Computed Midterm : %s",
                $gradeCalculator->getTentativePrelimResult(),
                $gradeCalculator->getComputedMidtermResult()
            );
            $cardCollection[] = Card::make($subject, $finalGrade);

        });
        /* loop and calculate each */
        return $cardCollection;
    }
    public static function canView(): bool
    {
        return false;
    }
}

