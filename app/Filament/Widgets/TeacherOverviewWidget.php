<?php

namespace App\Filament\Widgets;

use App\Models\User;
use Filament\Widgets\Widget;

class TeacherOverviewWidget extends Widget
{
    protected int|string|array $columnSpan = 'full';
    protected static string $view = 'filament.widgets.teacher-overview-widget';
    public static function canView(): bool
    {
        return auth()->user()->hasRole(User::TEACHER_ROLE);
    }
}
