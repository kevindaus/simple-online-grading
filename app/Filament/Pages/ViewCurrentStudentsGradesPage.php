<?php

namespace App\Filament\Pages;

use App\Filament\Resources\UserResource;
use App\Models\User;
use Filament\Forms\Components\Select;
use Filament\Pages\Actions\ButtonAction;
use Filament\Pages\Page;

class ViewCurrentStudentsGradesPage extends Page
{
    protected static ?string $navigationIcon = 'heroicon-o-document-text';

    protected static string $view = 'filament.pages.view-current-students-grades-page';

    protected static ?string $slug = "my-grade";

    protected static ?string $title = "My Grade";

    protected static function shouldRegisterNavigation(): bool
    {
        return auth()->user()->hasRole(User::STUDENT_ROLE);
    }

    public function mount()
    {
        abort_unless(auth()->user()->hasRole(User::STUDENT_ROLE), 403);
    }

    protected function getActions(): array
    {
        $that = $this;
        return [
            ButtonAction::make('printGrades')
                ->label("Print Grade")
                ->url(route('grade.export'))
                ->icon('heroicon-o-document-download'),
        ];
    }

}
