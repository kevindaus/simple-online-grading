<?php

namespace App\Filament\Pages;

use App\Models\User;
use Filament\Pages\Page;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Filters\SelectFilter;
use Filament\Tables\Table;

class Students extends Page
{
    protected static ?string $navigationIcon = 'heroicon-o-academic-cap';

    protected static string $view = 'filament.pages.students';
    protected static ?string $slug = 'students';
    protected static ?int $navigationSort = 2;
    public function mount()
    {
        abort_unless(auth()->user()->hasRole(User::ADMINISTRATOR_ROLE),403);
    }
    protected static function shouldRegisterNavigation(): bool
    {
        return auth()->user()->hasRole(User::ADMINISTRATOR_ROLE);
    }
    protected function getTitle(): string
    {
        return "Students";
    }
    protected function getBreadcrumbs(): array
    {
        return [
            "Students"
        ];
    }

}
