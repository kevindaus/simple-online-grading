<?php

namespace App\Filament\Pages;

use App\Models\Subject;
use App\Models\User;
use Filament\Pages\Page;

class ListEnrolledSubjects extends Page
{
    protected static ?string $navigationIcon = 'heroicon-o-folder';

    protected static string $view = 'filament.pages.list-enrolled-subjects';

    protected static ?string $slug = '/enrolled-subjects';
    protected static ?string $title = "Enrolled Subjects";
    private User $teacher;
    /**
     * @var Subject
     */
    private $subject;

    public function mount()
    {
        abort_unless(auth()->user()->hasRole(User::STUDENT_ROLE), 403);
        $this->teacher = User::role([User::TEACHER_ROLE])->where(['year_level' => auth()->user()->year_level])->first();
    }
    protected function getViewData(): array
    {
        return [
            'adviser' => $this->teacher->name
        ];
    }

    protected static function shouldRegisterNavigation(): bool
    {
        return auth()->user()->hasRole(User::STUDENT_ROLE);
    }


}
