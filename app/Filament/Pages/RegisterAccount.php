<?php

namespace App\Filament\Pages;

use App\Filament\Resources\UserResource\Pages\CreateUser;
use App\Models\User;
use Filament\Pages\Page;
use Filament\Resources\Form;

class RegisterAccount extends CreateUser
{
    protected static ?string $navigationIcon = 'heroicon-o-user';
    protected static ?int $navigationSort = 3;

    protected static function shouldRegisterNavigation(): bool
    {
        return auth()->user()->hasRole(User::ADMINISTRATOR_ROLE);
    }

}
