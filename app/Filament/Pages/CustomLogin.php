<?php

namespace App\Filament\Pages;

use DanHarrin\LivewireRateLimiting\Exceptions\TooManyRequestsException;
use Filament\Facades\Filament;
use Filament\Forms\Components\Checkbox;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\TextInput;
use Filament\Http\Livewire\Auth\Login;
use Illuminate\Contracts\View\View;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Str;

class CustomLogin extends Login
{
    public function mount(): void
    {
        if (Filament::auth()->check()) {
            redirect()->intended(Filament::getUrl());
        }

        $this->form->fill();
    }

    public function authenticate(): void
    {
        try {
            $this->rateLimit(5);
        } catch (TooManyRequestsException $exception) {
            $this->addError('email', __('filament::login.messages.throttled', [
                'seconds' => $exception->secondsUntilAvailable,
                'minutes' => ceil($exception->secondsUntilAvailable / 60),
            ]));

            return;
        }

        $data = $this->form->getState();

        if (!Filament::auth()->attempt([
            'identification_number' => $data['identification_number'],
            'password' => $data['password'],
        ], $data['remember'])) {
            $this->addError('email', __('filament::login.messages.failed'));
            return;
        }

        redirect()->intended(Filament::getUrl());
    }

    public function render(): View
    {
        return view('filament.pages.custom-login')
            ->layout('filament::components.layouts.base', [
                'title' => __('filament::login.title'),
            ]);
    }

    protected function getFormSchema(): array
    {
        $loginForms = [];
        $loginAs = Str::ucfirst(request('login_as'));

        $loginForms = [
            Placeholder::make('loginas')
                ->label('')
                ->content(new HtmlString('<h2 class="text-center" style="color: #1F209E;font-weight: bold;font-size: 1.5em " >'.$loginAs .'</h2>')),
            TextInput::make('identification_number')
                ->label(__('ID #'))
                ->required()
                ->autocomplete(),
            TextInput::make('password')
                ->label(__('filament::login.fields.password.label'))
                ->password()
                ->required(),
            Checkbox::make('remember')
                ->label(__('filament::login.fields.remember.label')),
        ];

        return $loginForms;

    }

}
