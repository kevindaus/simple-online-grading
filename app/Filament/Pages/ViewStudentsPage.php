<?php

namespace App\Filament\Pages;

use App\Models\User;
use Filament\Pages\Page;

class ViewStudentsPage extends Page
{
    protected static ?string $slug = "my-students";
    protected static ?string $title = "My Students";
    protected static ?string $navigationIcon = 'heroicon-o-academic-cap';

    protected static string $view = 'filament.pages.view-students-page';

    public function mount()
    {
        abort_unless(auth()->user()->hasRole(User::TEACHER_ROLE),403);
    }
    protected static function shouldRegisterNavigation(): bool
    {
        return auth()->user()->hasRole(User::TEACHER_ROLE);
    }
    protected function getBreadcrumbs(): array
    {
        return [
            "My Students"
        ];
    }
}
