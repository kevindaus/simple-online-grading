<?php

namespace App\Filament\Pages;

use App\Models\User;
use App\Settings\PerfectScoreSettings;
use Filament\Pages\SettingsPage;
use Filament\Forms;
use Filament\Resources\Form;

class ManagePerfectScore extends SettingsPage
{
    protected static ?int $navigationSort = 999;
    protected static ?string $navigationIcon = 'heroicon-o-cog';

    protected static string $settings = PerfectScoreSettings::class;
    protected static ?string $title = 'Score Settings';

    public function mount(): void
    {
        abort_unless(auth()->user()->hasRole(User::ADMINISTRATOR_ROLE), 403);
        parent::mount();
    }

    protected function getFormSchema(): array
    {
        $layout = Forms\Components\Card::class;
        return [
            $layout::make()
                ->schema([
                    Forms\Components\Placeholder::make('Prelim')->label("Prelim Perfect Score")->columnSpan(2),
                    Forms\Components\KeyValue::make('prelim_quizzes')->keyLabel("Quizzes")->valueLabel("Score")->columnSpan(2)->addButtonLabel("Add Quiz"),
                    Forms\Components\KeyValue::make('prelim_exams')->keyLabel("Exams")->valueLabel("Score")->columnSpan(2)->addButtonLabel("Add Exam"),
                    Forms\Components\TextInput::make('prelim_requirement')->numeric()->columnSpan(2)->columnSpan(2),
                ]),

            $layout::make()
                ->schema([
                    Forms\Components\Placeholder::make('Midterm')->label("Midterm Perfect Score"),
                    Forms\Components\KeyValue::make('midterm_quizzes')->keyLabel("Quizzes")->valueLabel("Score")->columnSpan(2)->addButtonLabel("Add Quiz"),
                    Forms\Components\KeyValue::make('midterm_exams')->keyLabel("Exams")->valueLabel("Score")->columnSpan(2)->addButtonLabel("Add Exam"),
                    Forms\Components\TextInput::make('midterm_requirement')->numeric()->columnSpan(2)->columnSpan(2),
                ]),

            $layout::make()
                ->schema([
                    Forms\Components\Placeholder::make('Finals')->label("Finals Perfect Score"),
                    Forms\Components\KeyValue::make('finals_quizzes')->keyLabel("Quizzes")->valueLabel("Score")->columnSpan(2)->addButtonLabel("Add Quiz"),
                    Forms\Components\KeyValue::make('finals_exams')->keyLabel("Exams")->valueLabel("Score")->columnSpan(2)->addButtonLabel("Add Exam"),
                    Forms\Components\TextInput::make('final_requirement')->numeric()->columnSpan(2)->columnSpan(2),
                ]),
        ];
    }

    protected static function shouldRegisterNavigation(): bool
    {
        return auth()->user()->hasRole([User::ADMINISTRATOR_ROLE]);
    }

}
