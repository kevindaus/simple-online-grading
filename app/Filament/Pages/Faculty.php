<?php

namespace App\Filament\Pages;

use App\Models\User;
use Filament\Pages\Page;

class Faculty extends Page
{
    protected static ?string $navigationIcon = 'heroicon-o-users';

    protected static string $view = 'filament.pages.faculty';

    protected static ?string $navigationLabel = "Instructors";

    protected static ?int $navigationSort = 1;


    protected static function shouldRegisterNavigation(): bool
    {
        return auth()->user()->hasRole(User::ADMINISTRATOR_ROLE);
    }
    protected function getTitle(): string
    {
        return "Instructors";
    }
    protected function getBreadcrumbs(): array
    {
        return [
            "Instructors"
        ];
    }

    public function mount()
    {
        abort_unless(auth()->user()->hasRole(User::ADMINISTRATOR_ROLE), 403);
    }

}
