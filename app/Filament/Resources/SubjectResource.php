<?php

namespace App\Filament\Resources;

use App\Filament\Resources\SubjectResource\Pages;
use App\Filament\Resources\SubjectResource\RelationManagers;
use App\Models\Grade;
use App\Models\Subject;
use App\Models\User;
use Filament\Forms;
use Filament\Forms\Components\Select;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Database\Eloquent\Builder;

class SubjectResource extends Resource
{
    protected static ?string $model = Subject::class;

    protected static ?string $recordTitleAttribute = 'name';

    protected static ?string $navigationIcon = 'heroicon-o-book-open';

    protected static ?int $navigationSort = 5;

    public static function form(Form $form): Form
    {
        $layout = Forms\Components\Card::class;

        return $form->schema([
            Forms\Components\Group::make()
                ->schema([
                    $layout::make()
                        ->schema([
                            Forms\Components\TextInput::make('name')->label("Subject"),
                            Forms\Components\MarkdownEditor::make('description') ,
                            Select::make('year_level')
                                ->options([
                                    User::FIRST_YEAR_LEVEL => '1st yr',
                                    User::SECOND_YEAR_LEVEL => '2nd yr',
                                    User::THIRD_YEAR_LEVEL => '3rd yr',
                                    User::FOURTH_YEAR_LEVEL => '4th yr',
                                ])->required(),
                            Select::make('semester')
                                ->options([
                                    Subject::FIRST_SEMESTER => '1st Semester',
                                    Subject::SECOND_SEMESTER => '2nd Semester',
                                ])->required(),
                        ])
                ])->columnSpan(2),
            Forms\Components\Group::make()
                ->schema([
                    $layout::make()
                        ->schema([
                            Forms\Components\Placeholder::make('created_at')
                                ->label('Created at')
                                ->content(fn(?Subject $record): string => $record ? $record->created_at->diffForHumans() : '-'),
                            Forms\Components\Placeholder::make('updated_at')
                                ->label('Last modified at')
                                ->content(fn(?Subject $record): string => $record ? $record->updated_at->diffForHumans() : '-'),
                        ])
                        ->columns(1),
                ])
                ->columnSpan(1),
        ])->columns(3);

    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('name')
                    ->label("Subject")
                    ->searchable()
                    ->sortable(),
//                TextColumn::make('description')
//                    ->searchable()
//                    ->sortable(),
                TextColumn::make('semester')
                    ->label("Semester")
                    ->enum([
                        ''=>'--',
                        Subject::FIRST_SEMESTER => '1st Semester',
                        Subject::SECOND_SEMESTER => '2nd Semester',
                    ])
                    ->searchable(),
                TextColumn::make('year_level')
                    ->label("Year")
                    ->enum([
                        ''=>'--',
                        User::FIRST_YEAR_LEVEL => '1st yr',
                        User::SECOND_YEAR_LEVEL => '2nd yr',
                        User::THIRD_YEAR_LEVEL => '3rd yr',
                        User::FOURTH_YEAR_LEVEL => '4th yr',
                    ])
                    ->searchable(),
//                TextColumn::make('created_at')
//                    ->label('Created at')
//                    ->date()
//                    ->sortable(),
            ])
            ->filters([
                SelectFilter::make('year_level')
                    ->options([
                        User::FIRST_YEAR_LEVEL => User::getYearLevelLabel()[User::FIRST_YEAR_LEVEL],
                        User::SECOND_YEAR_LEVEL => User::getYearLevelLabel()[User::SECOND_YEAR_LEVEL],
                        User::THIRD_YEAR_LEVEL => User::getYearLevelLabel()[User::THIRD_YEAR_LEVEL],
                        User::FOURTH_YEAR_LEVEL => User::getYearLevelLabel()[User::FOURTH_YEAR_LEVEL],
                    ]),
                SelectFilter::make('semester')
                    ->options([
                        Subject::FIRST_SEMESTER => '1st Semester',
                        Subject::SECOND_SEMESTER => '2nd Semester',
                    ]),

            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListSubjects::route('/'),
            'create' => Pages\CreateSubject::route('/create'),
            'edit' => Pages\EditSubject::route('/{record}/edit'),
            'showEnrollees'=> Pages\ShowEnrollees::route('/{record}/enrollees')
        ];
    }

    public static function getGloballySearchableAttributes(): array
    {
        return ['name'];
    }

    public static function getEloquentQuery(): Builder
    {
        if (auth()->user()->hasRole([User::ADMINISTRATOR_ROLE])) {
            return Subject::query();
        }else{
            return Subject::where(['year_level' => auth()->user()->year_level]);
        }

    }

}
