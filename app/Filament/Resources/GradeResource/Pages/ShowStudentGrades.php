<?php

namespace App\Filament\Resources\GradeResource\Pages;

use App\Filament\Resources\GradeResource;
use App\Models\User;
use Filament\Resources\Pages\Page;
use Filament\Tables\Actions\LinkAction;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use LaravelIdea\Helper\App\Models\_IH_User_C;
use LaravelIdea\Helper\App\Models\_IH_User_QB;

class ShowStudentGrades extends Page
{
    protected static string $resource = GradeResource::class;

    protected static string $view = 'filament.resources.grade-resource.pages.show-student-grades';

    /**
     * @var User|User[]|Builder|Builder[]|Collection|Model|_IH_User_C|_IH_User_QB|_IH_User_QB[]|null
     */
    private $currentStudent;
    protected static ?string $title = '';

    public function mount()
    {
        $this->currentStudent = User::with(['grades'])->find(request('student'));
        $pageTitle = sprintf("%s Grades", $this->currentStudent->name);
        static::$title = $pageTitle;
    }

    protected function getViewData(): array
    {

        return [
            'currentStudent' =>$this->currentStudent
        ];
    }

}
