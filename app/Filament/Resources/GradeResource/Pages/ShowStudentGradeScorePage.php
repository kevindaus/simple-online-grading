<?php

namespace App\Filament\Resources\GradeResource\Pages;

use App\Filament\Resources\GradeResource;
use App\Models\Grade;
use App\Models\Subject;
use App\Service\GradeCalculator;
use Filament\Resources\Pages\Page;
use Filament\Widgets\StatsOverviewWidget\Card;

class ShowStudentGradeScorePage extends Page
{
    protected static string $resource = GradeResource::class;

    protected static string $view = 'filament.resources.grade-resource.pages.show-student-grade-score-page';

    protected static ?string $slug = 'grades/{record}/show';
    protected static ?string $title = '';
    /**
     * @var Grade|Grade[]|\LaravelIdea\Helper\App\Models\_IH_Grade_C|null
     */
    private Grade $grade;

    public function mount()
    {
        $this->grade = Grade::find(request('record'));
        $pageTitle = sprintf("%s score for subject %s", ucwords($this->grade->user->name), ucwords($this->grade->subject->name));
        static::$title = $pageTitle;
    }
    protected function getHeaderWidgets(): array
    {
        return [

        ];
    }


    protected function getViewData(): array
    {
        $gradeCalculator = new GradeCalculator($this->grade);
        $computedFinalGrade = $gradeCalculator->getFinalGrade();
        $gradeCalculator->gradeViaPointSystem();
        $gradeDescription = $gradeCalculator->getGradeDescription();
        return [
            'grade'=>$this->grade,
            'gradeDescription'=>$gradeDescription,
            'final_grade' => $gradeCalculator->gradeViaPointSystem()
        ];
    }
}
