<?php

namespace App\Filament\Resources\GradeResource\Pages;

use App\Filament\Resources\GradeResource;
use App\Models\Grade;
use Closure;
use Filament\Resources\Pages\ListRecords;
use Filament\Tables;
use phpDocumentor\Reflection\Types\False_;

class ListGrades extends ListRecords
{
    protected static string $resource = GradeResource::class;
    protected function getTableRecordUrlUsing(): Closure
    {
        return function (Grade $record):?string {
            $resource = static::getResource();
            return $resource::getUrl('showStudentGrades', ['student' => $record->user_id]);
        };
    }
    protected function getEditAction(): Tables\Actions\Action
    {
        return(parent::getEditAction()->hidden(true));
    }
}
