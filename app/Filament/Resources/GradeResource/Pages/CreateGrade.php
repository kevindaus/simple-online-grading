<?php

namespace App\Filament\Resources\GradeResource\Pages;

use App\Filament\Resources\GradeResource;
use App\Models\Grade;
use App\Models\User;
use App\Settings\PerfectScoreSettings;
use Filament\Forms\Components\Card;
use Filament\Forms\Components\Group;
use Filament\Resources\Form;
use Filament\Resources\Pages\CreateRecord;

class CreateGrade extends CreateRecord
{
    protected static string $resource = GradeResource::class;


}
