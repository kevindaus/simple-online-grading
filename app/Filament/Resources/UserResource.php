<?php

namespace App\Filament\Resources;

use App\Filament\Resources\UserResource\Pages;
use App\Filament\Resources\UserResource\Pages\ShowFacultyEnrollees;
use App\Models\Grade;
use App\Models\User;
use App\Tables\Columns\RoleColumn;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Database\Eloquent\Builder;
use Spatie\Permission\Models\Role;

class UserResource extends Resource
{
    protected static ?string $model = User::class;

    protected static ?string $navigationIcon = 'heroicon-o-users';

    protected static ?string $recordTitleAttribute = 'name';

    protected static ?string $navigationLabel = "Student / Faculty";

    protected static bool $shouldRegisterNavigation = false;

    public static function form(Form $form): Form
    {
        $layout = Forms\Components\Card::class;
        return $form->schema([
            Forms\Components\Group::make()
                ->schema([
                    $layout::make()
                        ->schema([
                            Forms\Components\TextInput::make('identification_number')->required()->columnSpan(2),
                            Forms\Components\TextInput::make('name')->required(),
                            Forms\Components\TextInput::make('mobile_number')->required()->numeric(),
                            Forms\Components\Select::make('year_level')
                                ->options([
                                    User::FIRST_YEAR_LEVEL => str_replace("_", " ", ucwords(User::FIRST_YEAR_LEVEL)),
                                    User::SECOND_YEAR_LEVEL => str_replace("_", " ", ucwords(User::SECOND_YEAR_LEVEL)),
                                    User::THIRD_YEAR_LEVEL => str_replace("_", " ", ucwords(User::THIRD_YEAR_LEVEL)),
                                    User::FOURTH_YEAR_LEVEL => str_replace("_", " ", ucwords(User::FOURTH_YEAR_LEVEL)),
                                ])
                                ->required(),
                            Forms\Components\TextInput::make('course')->required(),
                            Forms\Components\TextInput::make('email')->required()->email(),
                        ])->columns([
                            'sm' => 2,
                        ]),
                    $layout::make()
                        ->schema([
                            Forms\Components\Select::make('role')
                                ->options([
                                    User::ADMINISTRATOR_ROLE => ucfirst(User::ADMINISTRATOR_ROLE),
                                    User::TEACHER_ROLE => ucfirst(User::TEACHER_ROLE),
                                    User::STUDENT_ROLE => ucfirst(User::STUDENT_ROLE),
                                ])->required(),
                            Forms\Components\TextInput::make('password')
                                ->label("Password")
                                ->required()
                                ->password()
                            ,
                            Forms\Components\TextInput::make('confirmpassword')
                                ->label("Confirm Password")
                                ->required()
                                ->password()
                                ->same('password')
                            ,
                        ])
                        ->columns(1),
                ])->columnSpan(2),
            Forms\Components\Group::make()
                ->schema([
                    $layout::make()
                        ->schema([
                            Forms\Components\Placeholder::make('created_at')
                                ->label('Created at')
                                ->content(fn(?User $record): string => $record ? $record->created_at->diffForHumans() : '-'),
                            Forms\Components\Placeholder::make('updated_at')
                                ->label('Last modified at')
                                ->content(fn(?User $record): string => $record ? $record->updated_at->diffForHumans() : '-'),
                        ])
                        ->columns(1),
                ])
                ->columnSpan(1),
        ])->columns(3);

    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('identification_number')
                    ->label("ID #")
                    ->searchable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('name')
                    ->searchable()
                    ->sortable(),
                TextColumn::make('year_level')
                    ->label("Year")
                    ->enum([
                        ''=>'--',
                        User::FIRST_YEAR_LEVEL => '1st yr',
                        User::SECOND_YEAR_LEVEL => '2nd yr',
                        User::THIRD_YEAR_LEVEL => '3rd yr',
                        User::FOURTH_YEAR_LEVEL => '4th yr',
                    ])
                    ->searchable(),
                TextColumn::make('roles.name')
                    ->label("Role")
                    ->searchable(),

            ])
            ->filters([
                SelectFilter::make('year_level')
                    ->options([
                        User::FIRST_YEAR_LEVEL => User::getYearLevelLabel()[User::FIRST_YEAR_LEVEL],
                        User::SECOND_YEAR_LEVEL => User::getYearLevelLabel()[User::SECOND_YEAR_LEVEL],
                        User::THIRD_YEAR_LEVEL => User::getYearLevelLabel()[User::THIRD_YEAR_LEVEL],
                        User::FOURTH_YEAR_LEVEL => User::getYearLevelLabel()[User::FOURTH_YEAR_LEVEL],
                    ]),
                SelectFilter::make('roles')
                        ->relationship('roles','name')
                    ->options([
                        User::STUDENT_ROLE =>  "Student",
                        User::TEACHER_ROLE => "Teacher",
                    ])
//                    ->column('roles.name')
            ]);
    }

    public static function getRelations(): array
    {
        return [

        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListUsers::route('/'),
            'create' => Pages\CreateUser::route('/create'),
            'edit' => Pages\EditUser::route('/{record}/edit'),
            'showStudentGrades' => Pages\ShowGrades::route('/{student}/grades'),
            'showEnrollees' => ShowFacultyEnrollees::route("/faculty/{faculty}/enrollees"),
        ];
    }


    public static function getGloballySearchableAttributes(): array
    {
        return ['name'];
    }

    public static function getEloquentQuery(): Builder
    {
        if (auth()->user()->hasRole([User::ADMINISTRATOR_ROLE])) {
            return User::with('roles');
        }else{
            return User::role([User::STUDENT_ROLE])->where(['year_level' => auth()->user()->year_level])->with('roles');
        }
    }

}
