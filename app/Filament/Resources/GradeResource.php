<?php

namespace App\Filament\Resources;


use App\Filament\Resources\GradeResource\Pages;
use App\Filament\Resources\GradeResource\Pages\ShowStudentGradeScorePage;
use App\Models\Grade;
use App\Models\User;
use App\Settings\PerfectScoreSettings;
use DB;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;

class GradeResource extends Resource
{
    protected static ?string $model = Grade::class;
    protected static ?int $navigationSort = 4;

    protected static ?string $navigationIcon = 'heroicon-o-calculator';


    public static function form(Form $form): Form
    {
        $layout = Forms\Components\Card::class;
        $perfectScoreSettings = new PerfectScoreSettings();
        // check if we are creating new record
        return $form->schema([
            Forms\Components\Group::make()
                ->schema([
                    $layout::make()
                        ->schema([
                            Forms\Components\BelongsToSelect::make('user')
                                ->label("Student")
                                ->relationship('user', 'name')
                                ->searchable()
                                ->required()
                                ->reactive()
                                ->preload()
                                ->afterStateUpdated(fn(callable $set, $state) => $set("year_level", User::find($state)->year_level ?: "")),
                            Forms\Components\BelongsToSelect::make('subject')
                                ->label("Subject")
                                ->relationship('subject', 'name')
                                ->preload()
                                ->searchable()
                                ->required(),
                        ]),
                    $layout::make()
                        ->schema([
                            Forms\Components\Placeholder::make('Prelim')->label("Prelim Perfect Score")->columnSpan(2),
                            Forms\Components\KeyValue::make('prelim_quizzes')->keyLabel("Quizzes")->valueLabel("Score")->columnSpan(2)->addButtonLabel("Add Quiz")
                                ->default( function(Component $livewire) use ($perfectScoreSettings) {
                                    if ($livewire instanceof Pages\CreateGrade) {
                                        return $perfectScoreSettings->prelim_quizzes;
                                    }
                                })
                                ->afterStateHydrated(function($component,$state) use ($perfectScoreSettings){
                                    $state = collect($perfectScoreSettings->prelim_quizzes)->mapWithKeys(function ($item ,$key) use($state) {
                                        return [
                                            $key=> $state[$key] ?? 0
                                        ];
                                    });
                                    $component->state($state->all());
                                }),
                            Forms\Components\KeyValue::make('prelim_exams')->keyLabel("Exams")->valueLabel("Score")->columnSpan(2)->addButtonLabel("Add Exam")
                                ->default( function(Component $livewire) use ($perfectScoreSettings) {
                                    if ($livewire instanceof Pages\CreateGrade) {
                                        return $perfectScoreSettings->prelim_exams;
                                    }
                                })
                                ->afterStateHydrated(function($component,$state) use ($perfectScoreSettings){
                                    $state = collect($perfectScoreSettings->prelim_exams)->mapWithKeys(function ($item ,$key) use($state) {
                                        return [
                                            $key=> $state[$key] ?? 0
                                        ];
                                    });
                                    $component->state($state->all());
                                }),
                            Forms\Components\TextInput::make('prelim_requirement')->numeric()->columnSpan(2)->columnSpan(2),

                        ]),
                    $layout::make()
                        ->schema([
                            Forms\Components\Placeholder::make('Midterm')->label("Midterm Perfect Score"),
                            Forms\Components\KeyValue::make('midterm_quizzes')->keyLabel("Quizzes")->valueLabel("Score")->columnSpan(2)->addButtonLabel("Add Quiz")
                                ->default( function(Component $livewire) use ($perfectScoreSettings) {
                                    if ($livewire instanceof Pages\CreateGrade) {
                                        return $perfectScoreSettings->midterm_quizzes;
                                    }
                                })
                                ->afterStateHydrated(function($component,$state) use ($perfectScoreSettings){
                                    $state = collect($perfectScoreSettings->midterm_quizzes)->mapWithKeys(function ($item ,$key) use($state) {
                                        return [
                                            $key=> $state[$key] ?? 0
                                        ];
                                    });
                                    $component->state($state->all());
                                }),
                            Forms\Components\KeyValue::make('midterm_exams')->keyLabel("Exams")->valueLabel("Score")->columnSpan(2)->addButtonLabel("Add Exam")
                                ->default( function(Component $livewire) use ($perfectScoreSettings) {
                                    if ($livewire instanceof Pages\CreateGrade) {
                                        return $perfectScoreSettings->midterm_exams;
                                    }
                                })
                                ->afterStateHydrated(function($component,$state) use ($perfectScoreSettings){
                                    $state = collect($perfectScoreSettings->midterm_exams)->mapWithKeys(function ($item ,$key) use($state) {
                                        return [
                                            $key=> $state[$key] ?? 0
                                        ];
                                    });
                                    $component->state($state->all());
                                }),
                            Forms\Components\TextInput::make('midterm_requirement')->numeric()->columnSpan(2)->columnSpan(2),
                        ]),
                    $layout::make()
                        ->schema([
                            Forms\Components\Placeholder::make('Finals')->label("Finals Perfect Score"),
                            Forms\Components\KeyValue::make('finals_quizzes')->keyLabel("Quizzes")->valueLabel("Score")->columnSpan(2)->addButtonLabel("Add Quiz")
                                ->default( function(Component $livewire) use ($perfectScoreSettings) {
                                    if ($livewire instanceof Pages\CreateGrade) {
                                        return $perfectScoreSettings->finals_quizzes;
                                    }
                                })
                                ->afterStateHydrated(function($component,$state) use ($perfectScoreSettings){
                                    $state = collect($perfectScoreSettings->finals_quizzes)->mapWithKeys(function ($item ,$key) use($state) {
                                        return [
                                            $key=> $state[$key] ?? 0
                                        ];
                                    });
                                    $component->state($state->all());
                                }),
                            Forms\Components\KeyValue::make('finals_exams')->keyLabel("Exams")->valueLabel("Score")->columnSpan(2)->addButtonLabel("Add Exam")
                                ->default( function(Component $livewire) use ($perfectScoreSettings) {
                                    if ($livewire instanceof Pages\CreateGrade) {
                                        return $perfectScoreSettings->finals_exams;
                                    }
                                })
                                ->afterStateHydrated(function($component,$state) use ($perfectScoreSettings){
                                    $state = collect($perfectScoreSettings->finals_exams)->mapWithKeys(function ($item ,$key) use($state) {
                                        return [
                                            $key=> $state[$key] ?? 0
                                        ];
                                    });
                                    $component->state($state->all());
                                }),
                            Forms\Components\TextInput::make('final_requirement')->numeric()->columnSpan(2)->columnSpan(2),
                        ])
                ])->columnSpan(2),

            Forms\Components\Group::make()
                ->schema([
                    $layout::make()
                        ->schema([
                            Forms\Components\Select::make('status')
                                ->label("Grade Status")
                                ->options([
                                    Grade::STATUS_COMPLETE => "Complete",
                                    Grade::STATUS_INCOMPLETE => "Incomplete",
                                    Grade::STATUS_OFFICIALLY_DROPPED => "Officially Dropped",
                                    Grade::STATUS_UNOFFICIALLY_DROPPED => "Unofficially Dropped",
                                ]),
                        ])
                        ->columns(1),
                    $layout::make()
                        ->schema([
                            Forms\Components\Select::make('semester')
                                ->label("Semester")
                                ->options([
                                    Grade::FIRST_SEMESTER => "First Semester",
                                    Grade::SECOND_SEMESTER => "Second Semester",
                                ]),
                            Forms\Components\Select::make('year_level')
                                ->options([
                                    User::FIRST_YEAR_LEVEL => User::getYearLevelLabel()[User::FIRST_YEAR_LEVEL],
                                    User::SECOND_YEAR_LEVEL => User::getYearLevelLabel()[User::SECOND_YEAR_LEVEL],
                                    User::THIRD_YEAR_LEVEL => User::getYearLevelLabel()[User::THIRD_YEAR_LEVEL],
                                    User::FOURTH_YEAR_LEVEL => User::getYearLevelLabel()[User::FOURTH_YEAR_LEVEL],
                                ])
                                ->reactive()
                                ->required(),
                        ])
                        ->columns(1),

                    $layout::make()
                        ->schema([

                            Forms\Components\Placeholder::make('created_at')
                                ->label('Created at')
                                ->content(fn(?Grade $record): string => $record ? $record?->created_at?->diffForHumans() : '-'),
                            Forms\Components\Placeholder::make('updated_at')
                                ->label('Last modified at')
                                ->content(fn(?Grade $record): string => $record ? $record->updated_at?->diffForHumans() : '-'),
                        ])
                        ->columns(1),

                ])
                ->columnSpan(1),
        ])->columns(3);

    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('user.name')->label("Student")->searchable(),
            ])
            ->filters([
                SelectFilter::make('year_level')
                    ->options([
                        User::FIRST_YEAR_LEVEL => User::getYearLevelLabel()[User::FIRST_YEAR_LEVEL],
                        User::SECOND_YEAR_LEVEL => User::getYearLevelLabel()[User::SECOND_YEAR_LEVEL],
                        User::THIRD_YEAR_LEVEL => User::getYearLevelLabel()[User::THIRD_YEAR_LEVEL],
                        User::FOURTH_YEAR_LEVEL => User::getYearLevelLabel()[User::FOURTH_YEAR_LEVEL],
                    ])
            ])->prependActions([

            ]);
    }


    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListGrades::route('/'),
            'create' => Pages\CreateGrade::route('/create'),
            'edit' => Pages\EditGrade::route('/{record}/edit'),
            'show' => ShowStudentGradeScorePage::route('/{record}/show'),
            'showStudentGrades' => Pages\ShowStudentGrades::route('/{student}/grades'),
        ];
    }

    public static function getEloquentQuery(): Builder
    {
        $query = Grade::query();
        $query->with('user')
            ->select([
                'user_id',
                'id',
                'subject_id',
                'semester',
                'year_level',
                'status',
                'prelim_quizzes',
                'prelim_exams',
                'midterm_quizzes',
                'midterm_exams',
                'finals_quizzes',
                'finals_exams',

                'prelim_quiz_1',
                'prelim_quiz_2',
                'prelim_quiz_3',
                'prelim_exam',
                'prelim_requirement',
                'midterm_quiz_1',
                'midterm_quiz_2',
                'midterm_quiz_3',
                'midterm_exam',
                'midterm_requirement',
                'final_quiz_1',
                'final_quiz_2',
                'final_quiz_3',
                'final_exam',
                'final_requirement',
                'created_at',
                'updated_at',
            ])
            ->groupBy(['user_id'])
            ->orderBy(DB::raw('id,semester,subject_id,year_level,status,prelim_quiz_1,prelim_quiz_2,prelim_quiz_3,prelim_exam,prelim_requirement,midterm_quiz_1,midterm_quiz_2,midterm_quiz_3,midterm_exam,midterm_requirement,final_quiz_1,final_quiz_2,final_quiz_3,final_exam,final_requirement,created_at,updated_at'));
        if (auth()->user()->hasRole(User::TEACHER_ROLE)) {
            $query->where([
                'year_level' => auth()->user()->year_level
            ]);
        }
        return $query;

    }

    protected function getTableFilters(): array
    {
        return [

        ];
    }

}
