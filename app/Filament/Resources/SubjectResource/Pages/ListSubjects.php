<?php

namespace App\Filament\Resources\SubjectResource\Pages;

use App\Filament\Resources\SubjectResource;
use App\Models\Subject;
use App\Models\User;
use Filament\Resources\Pages\ListRecords;

class ListSubjects extends ListRecords
{
    protected static string $resource = SubjectResource::class;
    protected function getTableRecordUrlUsing(): \Closure
    {
        /* if the user is student */
        return function (Subject $record): ?string {
            $resource = static::getResource();
            return $resource::getUrl('showEnrollees', $record);
        };
    }
}
