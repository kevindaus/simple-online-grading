<?php

namespace App\Filament\Resources\SubjectResource\Pages;

use App\Filament\Resources\SubjectResource;
use App\Models\Subject;
use App\Models\User;
use Filament\Resources\Pages\Page;

class ShowEnrollees extends Page
{
    protected static string $resource = SubjectResource::class;

    protected static string $view = 'filament.resources.subject-resource.pages.show-enrollees';
    protected static ?string $title = '';
    /**
     * @var Subject|Subject[]|\LaravelIdea\Helper\App\Models\_IH_Subject_C
     */
    private $subject;

    public function mount()
    {
        $this->subject = Subject::findOrFail(request('record'));
        $pageTitle = sprintf("%s Enrollees", $this->subject->name);
        static::$title = $pageTitle;
    }

    protected function getViewData(): array
    {
        return [
            'currentSubject' => $this->subject
        ];
    }
}
