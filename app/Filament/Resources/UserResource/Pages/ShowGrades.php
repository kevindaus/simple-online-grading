<?php

namespace App\Filament\Resources\UserResource\Pages;

use App\Filament\Resources\UserResource;
use App\Models\User;
use Filament\Pages\Actions\ButtonAction;
use Filament\Resources\Pages\Page;


class ShowGrades extends Page
{
    protected static string $resource = UserResource::class;

    protected static string $view = 'filament.resources.user-resource.pages.show-grades';
    protected static ?string $title = '';
    /**
     * @var User|User[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\LaravelIdea\Helper\App\Models\_IH_User_C|\LaravelIdea\Helper\App\Models\_IH_User_QB|\LaravelIdea\Helper\App\Models\_IH_User_QB[]|null
     */
    private $currentStudent;

    public function mount()
    {
        $this->currentStudent = User::with(['grades'])->find(request('student'));
        $pageTitle = sprintf("%s", $this->currentStudent->name);
        static::$title = $pageTitle;
    }

    protected function getViewData(): array
    {
        return [
            'currentStudent' => $this->currentStudent
        ];
    }
    protected function getActions(): array
    {
        if (auth()->user()->hasRole(User::ADMINISTRATOR_ROLE)) {
            return [
                ButtonAction::make('Edit Profile')->url(UserResource::getUrl('edit',['record'=>$this->currentStudent])),
            ];
        }
        return [];
    }
}
