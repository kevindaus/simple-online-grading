<?php

namespace App\Filament\Resources\UserResource\Pages;

use App\Filament\Resources\UserResource;
use App\Models\User;
use Filament\Forms\Components\Card;
use Filament\Forms\Components\Group;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Pages\EditRecord;
use Hash;

class EditUser extends EditRecord
{
    protected static string $resource = UserResource::class;
    public User $user;
    protected function mutateFormDataBeforeFill(array $data): array
    {
        $data['role'] = $this->record->roles()->first()->name;


        return $data;
    }


    protected function mutateFormDataBeforeSave(array $data): array
    {
        if (isset($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        }else{
            unset($data['password']);
        }
        return $data;
    }
    protected function form(Form $form): Form
    {
        $layout = Card::class;
        return $form->schema([
            Group::make()
                ->schema([
                    $layout::make()
                        ->schema([
                            TextInput::make('identification_number')->required()->columnSpan(2),
                            TextInput::make('name')->required(),
                            TextInput::make('mobile_number')->required()->numeric(),
                            Select::make('year_level')
                                ->options([
                                    User::FIRST_YEAR_LEVEL => str_replace("_", " ", ucwords(User::FIRST_YEAR_LEVEL)),
                                    User::SECOND_YEAR_LEVEL => str_replace("_", " ", ucwords(User::SECOND_YEAR_LEVEL)),
                                    User::THIRD_YEAR_LEVEL => str_replace("_", " ", ucwords(User::THIRD_YEAR_LEVEL)),
                                    User::FOURTH_YEAR_LEVEL => str_replace("_", " ", ucwords(User::FOURTH_YEAR_LEVEL)),
                                ])
                                ->required(),
                            TextInput::make('course')->required(),
                            TextInput::make('email')->email(),
                        ])->columns([
                            'sm' => 2,
                        ]),
                    $layout::make()
                        ->schema([
                            Select::make('role')
                                ->options([
                                    User::ADMINISTRATOR_ROLE => ucfirst(User::ADMINISTRATOR_ROLE),
                                    User::TEACHER_ROLE => ucfirst(User::TEACHER_ROLE),
                                    User::STUDENT_ROLE => ucfirst(User::STUDENT_ROLE),
                                ])->required(),
                            TextInput::make('password')
                                ->label("Password")
                                ->password()
                            ,
                            TextInput::make('confirmpassword')
                                ->label("Confirm Password")
                                ->password()
                                ->same('password')
                            ,
                        ])
                        ->columns(1),
                ])->columnSpan(2),
            Group::make()
                ->schema([
                    $layout::make()
                        ->schema([
                            Placeholder::make('created_at')
                                ->label('Created at')
                                ->content(fn(?User $record): string => $record ? $record->created_at->diffForHumans() : '-'),
                            Placeholder::make('updated_at')
                                ->label('Last modified at')
                                ->content(fn(?User $record): string => $record ? $record->updated_at->diffForHumans() : '-'),
                        ])
                        ->columns(1),
                ])
                ->columnSpan(1),
        ])->columns(3);

    }
}
