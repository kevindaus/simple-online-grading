<?php

namespace App\Filament\Resources\UserResource\Pages;

use App\Filament\Resources\UserResource;
use App\Models\User;
use Filament\Resources\Pages\CreateRecord;
use Spatie\Permission\Models\Role;

class CreateUser extends CreateRecord
{
    protected static string $resource = UserResource::class;
    /**
     * @var mixed
     */
    private $name;
    /**
     * @var mixed
     */
    private $email;
    /**
     * @var mixed
     */
    private $identification_number;
    /**
     * @var mixed
     */
    private $role;

    protected function mutateFormDataBeforeCreate(array $data): array
    {
        $this->name = $data['name'];
        $this->email = $data['email'];
        $this->identification_number = $data['identification_number'];
        $this->role = $data['role'];
        $data['password'] = \Hash::make($data['password']);
        return $data;
    }

    protected function afterCreate()
    {
        /* @var $newUser User */
        $newUser = User::where([
            'name' => $this->name,
            'email' => $this->email,
            'identification_number' => $this->identification_number
        ])->firstOrFail();
        $newUser->syncRoles(Role::findOrCreate($this->role));
    }
}
