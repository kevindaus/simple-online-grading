<?php

namespace App\Filament\Resources\UserResource\Pages;

use App\Filament\Resources\UserResource;
use App\Models\User;
use Filament\Resources\Pages\Page;

class ShowFacultyEnrollees extends Page
{
    protected static string $resource = UserResource::class;

    protected static string $view = 'filament.resources.user-resource.pages.show-faculty-enrollees';
    protected static ?string $title = "";

    private User $faculty;

    public function mount()
    {
        $this->faculty = User::find(request('faculty'));
        $customTitle = sprintf("Enrollees under %s", $this->faculty->name);
        static::$title = $customTitle;
    }
    protected function getViewData(): array
    {
        return [
            'faculty' => $this->faculty
        ];
    }


}
