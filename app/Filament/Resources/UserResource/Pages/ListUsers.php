<?php

namespace App\Filament\Resources\UserResource\Pages;

use App\Filament\Resources\UserResource;
use App\Models\User;
use Filament\Resources\Pages\ListRecords;

class ListUsers extends ListRecords
{
    protected static string $resource = UserResource::class;
    protected static ?string $title = "Student / Faculty";

    protected function getTableRecordUrlUsing(): \Closure
    {
        /* if the user is student */
        return function (User $record): ?string {
            $resource = static::getResource();
            if ($record->hasAnyRole([User::TEACHER_ROLE])) {
                return $resource::getUrl('showEnrollees', ['faculty' => $record->id]);
            }else if ($record->hasAnyRole([User::STUDENT_ROLE])) {
                return $resource::getUrl('showStudentGrades', ['student' => $record->id]);
            }
            return "";
        };
    }
}
