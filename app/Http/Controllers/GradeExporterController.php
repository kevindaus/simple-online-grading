<?php

namespace App\Http\Controllers;

use App\Models\Grade;
use App\Models\User;
use App\Service\GradeCalculator;
use Barryvdh\DomPDF\Facade\Pdf;
use Spatie\Permission\Models\Role;


class GradeExporterController extends Controller
{
    public function index()
    {
        /*get all the grades owned by current student */
        $groupedGrades = [];
        $grades = Grade::where(['user_id' => auth()->user()->id])
            ->get()
            ->map(function (Grade $item) use (&$groupedGrades) {
                $semester = \App\Models\Grade::getSemesterLabel()[$item->subject->semester] .' 2020-2021';
                if ($item->subject->semester === Grade::SECOND_SEMESTER) {
                    $semester = \App\Models\Grade::getSemesterLabel()[$item->subject->semester] .' 2021-2022';
                }
                $groupedGrades[$semester][] = $item;
                return $item;
            });
        $currentInstructor = User::role([Role::findOrCreate(User::TEACHER_ROLE)])->where(['year_level' => auth()->user()->year_level])->firstOrFail();

        $pdf = PDF::loadView("export.grades", [
            'courseAndYear' =>  sprintf("%s : %s" , auth()->user()->course , \Str::of(User::getYearLevelLabel()[auth()->user()->year_level])->headline()  ) ,
            'idNumber' => auth()->user()->identification_number,
            'studentName' => auth()->user()->name,
            'grades' => $groupedGrades,
            'currentInstructor'=>$currentInstructor->name,
            'nvsu_logo' => base64_encode(file_get_contents(public_path('images/nvsu.jpg'))),
            'nvsu_it_logo' => base64_encode(file_get_contents(public_path('images/nvsu_it2.png'))),
        ]);
//        return $pdf->download('My Grades.pdf');
        return $pdf->stream('My Grades.pdf');

    }
}
