<?php

namespace App\Http\Livewire;

use App\Filament\Resources\UserResource;
use App\Models\User;
use Filament\Tables\Columns\BadgeColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Concerns\InteractsWithTable;
use Filament\Tables\Contracts\HasTable;
use Filament\Tables\Filters\SelectFilter;
use Livewire\Component;

class ListStudentTable extends Component implements HasTable
{
    use InteractsWithTable;

    public function render()
    {
        return view('livewire.list-student-table');
    }
    public function getTableQuery(): \Illuminate\Database\Eloquent\Builder
    {
        if (auth()->user()->hasRole(User::ADMINISTRATOR_ROLE)) {
            return User::role(User::STUDENT_ROLE);
        }
        return User::role(User::STUDENT_ROLE)->where(['year_level'=>auth()->user()->year_level]);
    }
    protected function getTableColumns(): array
    {
        return [
            TextColumn::make('identification_number')->label("ID #"),
            TextColumn::make('name')->label("Student")->searchable(),
            TextColumn::make('course')->label("Course")->searchable(),
            BadgeColumn::make('year_level')
                ->label("Year Level")
                ->enum([
                    null => '',
                    User::FIRST_YEAR_LEVEL => User::getYearLevelLabel()[User::FIRST_YEAR_LEVEL],
                    User::SECOND_YEAR_LEVEL => User::getYearLevelLabel()[User::SECOND_YEAR_LEVEL],
                    User::THIRD_YEAR_LEVEL => User::getYearLevelLabel()[User::THIRD_YEAR_LEVEL],
                    User::FOURTH_YEAR_LEVEL => User::getYearLevelLabel()[User::FOURTH_YEAR_LEVEL],
                ]),
        ];
    }
    protected function getTableFilters(): array
    {
        return [
            SelectFilter::make('year_level')
            ->options([
                User::FIRST_YEAR_LEVEL => User::getYearLevelLabel()[User::FIRST_YEAR_LEVEL],
                User::SECOND_YEAR_LEVEL => User::getYearLevelLabel()[User::SECOND_YEAR_LEVEL],
                User::THIRD_YEAR_LEVEL => User::getYearLevelLabel()[User::THIRD_YEAR_LEVEL],
                User::FOURTH_YEAR_LEVEL => User::getYearLevelLabel()[User::FOURTH_YEAR_LEVEL],
            ])

        ];
    }

    protected function getTableRecordUrlUsing(): \Closure
    {
        /* if the user is student */
        return function (User $record): ?string {
            $resource = UserResource::class;
            if ($record->hasAnyRole([User::TEACHER_ROLE])) {
                return $resource::getUrl('showEnrollees', ['faculty' => $record->id]);
            }else if ($record->hasAnyRole([User::STUDENT_ROLE])) {
                return $resource::getUrl('showStudentGrades', ['student' => $record->id]);
            }

        };
    }
}
