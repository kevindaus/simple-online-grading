<?php

namespace App\Http\Livewire;

use App\Models\Grade;
use App\Models\Subject;
use App\Models\User;
use Livewire\Component;
use Spatie\Permission\Models\Role;

class TeacherDashboard extends Component
{
    public $year_level;
    public $student_count;
    public $subject_count;
    public $grade_count;

    public function mount()
    {
        $currentTeacher = auth()->user();
        $this->year_level = $currentTeacher->year_level;
        $this->student_count = User::role([Role::findOrCreate(User::STUDENT_ROLE)])->where(['year_level' => $this->year_level])->count();
        $this->subject_count = Subject::where(['year_level' => $this->year_level])->count();
        $this->grade_count = Grade::where(['year_level' => $this->year_level])->count();
    }
    public function render()
    {
        return view('livewire.teacher-dashboard');
    }
}
