<?php

namespace App\Http\Livewire;

use App\Models\Grade;
use App\Service\GradeCalculator;
use Livewire\Component;

class ListCurrentStudentGrade extends Component
{
    public array $grades = [];
    public function mount()
    {
        $myGrades = auth()->user()->grades()->get();


        foreach ($myGrades as $currentGrade) {
            /* @var $currentGrade Grade */
            $gradeCalculator = new GradeCalculator($currentGrade);
            $finalGrade = $gradeCalculator->gradeViaPointSystem();

            $this->grades[] = [
                'subject'=>  $currentGrade->subject->name,
                'semester'=>  $currentGrade->semester,
                'grade'=> $finalGrade
            ];
        }
    }
    public function render()
    {
        return view('livewire.list-current-student-grade');
    }
}
