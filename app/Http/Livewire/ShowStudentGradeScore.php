<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ShowStudentGradeScore extends Component
{
    public $grade;
    public function render()
    {
        return view('livewire.show-student-grade-score');
    }
}
