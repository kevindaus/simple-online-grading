<?php

namespace App\Http\Livewire;

use App\Filament\Resources\GradeResource;
use Closure;
use App\Models\Grade;
use App\Models\Shop\Customer;
use App\Models\User;
use App\Tables\Columns\GradeColumn;
use Filament\Tables\Actions\LinkAction;
use Filament\Tables\Columns\BadgeColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Concerns\InteractsWithTable;
use Filament\Tables\Contracts\HasTable;
use Filament\Tables\Filters\SelectFilter;
use Livewire\Component;

class CurrentStudentGrades extends Component implements HasTable
{
    use InteractsWithTable;

    /**
     * @var User
     */
    public $student;

    public function render()
    {
        return view('livewire.current-student-grades');
    }

    public function getTableQuery(): \Illuminate\Database\Eloquent\Builder
    {

        return Grade::query()->where([
            'user_id'=> $this->student->id
        ]);
    }

    protected function getTableColumns(): array
    {
        return [
            TextColumn::make('subject.name')->label("Subject")->searchable(),
            TextColumn::make('subject.description')->label("Description")->searchable(),
            TextColumn::make('semester')
                ->label("Semester")
                ->enum([
                    ''=>'--',
                    Grade::FIRST_SEMESTER => '1st sem',
                    Grade::SECOND_SEMESTER => '2nd sem',
                ])
                ->searchable(),
            GradeColumn::make('id')->label("Grade")->searchable(),
        ];
    }
    protected function getTableFilters(): array
    {
        return [
            SelectFilter::make('semester')
                ->options([
                    Grade::FIRST_SEMESTER => '1st sem',
                    Grade::SECOND_SEMESTER => '2nd sem',
                ])

        ];
    }
    protected function getTableActions(): array
    {
        if (auth()->user()->hasRole(User::ADMINISTRATOR_ROLE)) {
            return [
                LinkAction::make('edit')
                    ->label("Edit")
                    ->url(fn (Grade $record): string => GradeResource::getUrl('edit',['record' => $record])  )
                    ->openUrlInNewTab()
            ];
        }
        return [];
    }

    protected function getTableRecordUrlUsing(): ?Closure
    {
        return function (Grade $record): ?string {
            $resource = GradeResource::class;
            return $resource::getUrl('show', ['record' => $record]);
        };
    }
}
