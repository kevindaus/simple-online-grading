<?php

namespace App\Http\Livewire;



use App\Models\Grade;
use App\Models\Subject;
use App\Models\User;
use Filament\Tables\Columns\BadgeColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Concerns\InteractsWithTable;
use Filament\Tables\Contracts\HasTable;
use Livewire\Component;

class ShowAllEnrollees extends Component implements HasTable
{
    use InteractsWithTable;

    public Subject $subject;

    public function render()
    {
        return view('livewire.show-all-enrollees');
    }

    public function getTableQuery(): \Illuminate\Database\Eloquent\Builder
    {
        return Grade::with('user')->where(['subject_id' => $this->subject->id]);
    }
    protected function getTableColumns(): array
    {
        return [
            TextColumn::make('user.name')->label("Enrollee"),

        ];
    }
}
