<?php

namespace App\Http\Livewire;

use App\Models\Grade;
use App\Models\User;
use Filament\Tables\Columns\BadgeColumn;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Concerns\InteractsWithTable;
use Filament\Tables\Contracts\HasTable;
use Illuminate\Database\Eloquent\Builder;
use Livewire\Component;

class ListEnrolledSubjects extends Component implements HasTable
{
    use InteractsWithTable;

    public User $student;

    public function render()
    {
        return view('livewire.list-enrolled-subjects');
    }

    public function getTableQuery(): Builder
    {
        return Grade::with(['subject'])
            ->where(['user_id' => $this->student->id])
            ->groupBy('subject_id');
    }
    protected function getTableColumns(): array
    {
        return [
            TextColumn::make('subject.name')->label("Subject")->searchable(),
            TextColumn::make('subject.description')->label("Description")->searchable(),
        ];
    }
}
