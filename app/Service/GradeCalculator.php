<?php

namespace App\Service;

use App\Models\Grade;
use App\Settings\PerfectScoreSettings;

class GradeCalculator
{
    protected Grade $grade;
    /**
     * @var float
     */
    private $finalGrade;
    /**
     * @var float|int
     */
    private $computedMidtermResult;
    /**
     * @var float|int
     */
    private $tentativePrelimResult;

    private string $gradeDescription = '';

    /**
     * @param Grade $grade
     */
    public function __construct(Grade $grade)
    {
        $this->grade = $grade;
        $this->computeGrade();
    }


    /**
     * @return string
     */
    public function getGradeDescription(): string
    {
        return $this->gradeDescription;
    }

    /**
     * @param string $gradeDescription
     */
    public function setGradeDescription(string $gradeDescription): void
    {
        $this->gradeDescription = $gradeDescription;
    }

    public function computeGrade()
    {
        $perfectScoreSettings = new PerfectScoreSettings();
        $totalPerfectPrelimQuiz = collect($perfectScoreSettings->prelim_quizzes)->sum();
        $prelimRequirementPerfect = $perfectScoreSettings->prelim_requirement;
        $prelimExamPerfect = collect($perfectScoreSettings->prelim_exams)->sum();

        $prelimQuizTotal = collect($this->grade->prelim_exam)->sum();
        $percentagePrelimQuiz = ((float)($prelimQuizTotal / $totalPerfectPrelimQuiz) * 60 + 40);
        $percentageRequirement = ((float)($this->grade->prelim_requirement / $prelimRequirementPerfect) * 60 + 40);
        $totalPrelimExams = collect($this->grade->prelim_exams)->sum();
        $percentageFinal = ((float)(  $totalPrelimExams / $prelimExamPerfect) * 60 + 40);
        $this->tentativePrelimResult = (($percentagePrelimQuiz * 0.3)) +
            (($percentageRequirement * 0.5)) +
            (($percentageFinal * 0.2));


        $totalMidtermPerfectQuiz = collect($perfectScoreSettings->midterm_quizzes)->sum();
        $perfectMidtermRequirement = $perfectScoreSettings->midterm_requirement;
        $perfectMidtermExamRequirement = collect($perfectScoreSettings->midterm_exams)->sum();

        $midtermQuizTotal = collect($this->grade->midterm_quizzes)->sum();
        $percentageMidtermQuiz = ((float)($midtermQuizTotal / $totalMidtermPerfectQuiz) * 60 + 40);
        $percentageMidtermRequirement = ((float)($this->grade->midterm_requirement / $perfectMidtermRequirement) * 60 + 40);
        $midtermExamsTotal = collect($this->grade->midterm_exams)->sum();
        $midtermPercentageFinal = ((float)(  $midtermExamsTotal / $perfectMidtermExamRequirement) * 60 + 40);

        $tentativeMidtermResult = (($percentageMidtermQuiz * 0.3)) +
            (($percentageMidtermRequirement * 0.5)) +
            (($midtermPercentageFinal * 0.2));

        $this->computedMidtermResult = ($tentativeMidtermResult + $this->tentativePrelimResult) / 2;

        $totalPerfectFinalQuiz = collect($perfectScoreSettings->finals_quizzes)->sum();
        $perfectFinalRequirement = $perfectScoreSettings->final_requirement;
        $perfectFinalExam = collect($perfectScoreSettings->finals_exams)->sum();

        $finalsQuizTotal = collect($this->grade->finals_quizzes)->sum();
        $finalPercentageQuiz = ((float)($finalsQuizTotal / $totalPerfectFinalQuiz) * 60 + 40);
        $finalPercentageRequirement = ((float)($this->grade->final_requirement / $perfectFinalRequirement) * 60 + 40);
        $totalFinalsExams = collect($this->grade->finals_exams)->sum();
        $finalPercentageExam = ((float)(  $totalFinalsExams / $perfectFinalExam) * 60 + 40);
        $tentativeFinalResult =
            ((float)$finalPercentageQuiz * 0.3) +
            ((float)$finalPercentageRequirement * 0.5) +
            ((float)$finalPercentageExam * 0.2);
        $this->finalGrade = (float)$tentativeFinalResult * 0.6 + ($this->computedMidtermResult) * 0.4;
    }
    public function getFinalGrade()
    {
        return $this->finalGrade;
    }

    public function gradeViaPointSystem()
    {
        if (in_array($this->grade->status, [Grade::STATUS_OFFICIALLY_DROPPED, Grade::STATUS_UNOFFICIALLY_DROPPED, Grade::STATUS_INCOMPLETE])) {
            if ($this->grade->status === Grade::STATUS_OFFICIALLY_DROPPED) {
                $this->gradeDescription = "Officially Dropped without Credit";
            } else if ($this->grade->status === Grade::STATUS_UNOFFICIALLY_DROPPED) {
                $this->gradeDescription = "Unofficially Dropped without Credit";
            } else if ($this->grade->status === Grade::STATUS_INCOMPLETE) {
                $this->gradeDescription = "Incomplete";
            }
            return $this->grade->status;
        }

        $pointGrade = 5;
        if ($this->finalGrade <= 58.49) {
            $this->gradeDescription = "Failed";
            return 5;
        } else if ($this->finalGrade <= 74.99) {
            $this->gradeDescription = "Conditional if Pass/Failed";
            return 4;
        } else if ($this->finalGrade <= 76) {
            $this->gradeDescription = "Fair/Pass";
            return 3;
        } else if ($this->finalGrade <= 78) {
            $this->gradeDescription = "Satisfactory";
            return 2.75;
        } else if ($this->finalGrade <= 80) {
            $this->gradeDescription = "Satisfactory";
            return 2.50;
        } else if ($this->finalGrade <= 83) {
            $this->gradeDescription = "Satisfactory";
            return 2.25;
        } else if ($this->finalGrade <= 85) {
            $this->gradeDescription = "Very Satisfactory";
            return 2;
        } else if ($this->finalGrade <= 87) {
            $this->gradeDescription = "Very Satisfactory";
            return 1.75;
        } else if ($this->finalGrade <= 90) {
            $this->gradeDescription = "Very Satisfactory";
            return 1.50;
        } else if ($this->finalGrade <= 94) {
            $this->gradeDescription = "Outstanding";
            return 1.25;
        } else {
            $this->gradeDescription = "Outstanding";
            return 1;
        }

    }


    /**
     * @param Grade $grade
     */
    public function setGrade(Grade $grade): void
    {
        $this->grade = $grade;
    }

    /**
     * @return float|int
     */
    public function getComputedMidtermResult()
    {
        return $this->computedMidtermResult;
    }

    /**
     * @return float|int
     */
    public function getTentativePrelimResult()
    {
        return $this->tentativePrelimResult;
    }


}
