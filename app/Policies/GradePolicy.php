<?php

namespace App\Policies;

use App\Models\Grade;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GradePolicy
{
    use HandlesAuthorization;


    public function viewAny(User $user)
    {
        return $user->hasAnyRole([User::TEACHER_ROLE, User::ADMINISTRATOR_ROLE]);
    }

    public function view(User $user, Grade $grade)
    {
        $isAdmin = $user->hasAnyRole(User::ADMINISTRATOR_ROLE);
        $isTeacher = $user->hasAnyRole(User::TEACHER_ROLE);
        $userOwnsGrade = $user->id === $grade->user_id;
        return $isAdmin || $isTeacher || $userOwnsGrade;
    }

    public function create(User $user)
    {
        return $user->hasAnyRole([User::TEACHER_ROLE, User::ADMINISTRATOR_ROLE]);
    }

    public function update(User $user, Grade $grade)
    {
        return $user->hasAnyRole([User::TEACHER_ROLE, User::ADMINISTRATOR_ROLE]);
    }

    public function delete(User $user, Grade $grade)
    {
        return $user->hasAnyRole([User::TEACHER_ROLE, User::ADMINISTRATOR_ROLE]);
    }

    public function restore(User $user, Grade $grade)
    {
        return $user->hasAnyRole([User::TEACHER_ROLE, User::ADMINISTRATOR_ROLE]);
    }

    public function forceDelete(User $user, Grade $grade)
    {
        return $user->hasAnyRole([User::TEACHER_ROLE, User::ADMINISTRATOR_ROLE]);
    }

    public function show(User $user, Grade $grade)
    {
        return $user->hasRole([User::ADMINISTRATOR_ROLE, User::TEACHER_ROLE]);
    }

    public function showStudentGrades(User $user, Grade $grade)
    {
        return $user->hasRole([User::ADMINISTRATOR_ROLE, User::TEACHER_ROLE]);
    }
}
