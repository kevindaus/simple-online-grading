<?php

namespace App\Policies;

use App\Models\Subject;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SubjectPolicy
{
    use HandlesAuthorization;

    public function __construct()
    {
        //
    }

    public function viewAny(User $user)
    {
        return $user->hasAnyRole([User::ADMINISTRATOR_ROLE,User::TEACHER_ROLE]);
    }

    public function view(User $user, Subject $subject)
    {
        return $user->hasAnyRole([User::ADMINISTRATOR_ROLE,User::TEACHER_ROLE]);
    }

    public function create(User $user)
    {
        return $user->hasAnyRole(User::ADMINISTRATOR_ROLE);
    }

    public function update(User $user, Subject $subject)
    {
        return $user->hasAnyRole(User::ADMINISTRATOR_ROLE);
    }

    public function delete(User $user, Subject $subject)
    {
        return $user->hasAnyRole(User::ADMINISTRATOR_ROLE);
    }

    public function restore(User $user, Subject $subject)
    {
        return $user->hasAnyRole(User::ADMINISTRATOR_ROLE);
    }

    public function forceDelete(User $user, Subject $subject)
    {
        return $user->hasAnyRole(User::ADMINISTRATOR_ROLE);
    }
}
