<?php

namespace App\Settings;

use Spatie\LaravelSettings\Settings;

class PerfectScoreSettings extends Settings
{
    public array $prelim_quizzes;
    public array $prelim_exams;
    public array $midterm_quizzes;
    public array $midterm_exams;
    public array $finals_quizzes;
    public array $finals_exams;

    public float $prelim_quiz_1;
    public float $prelim_quiz_2;
    public float $prelim_quiz_3;
    public float $prelim_exam;
    public float $prelim_requirement;
    public float $midterm_quiz_1;
    public float $midterm_quiz_2;
    public float $midterm_quiz_3;
    public float $midterm_exam;
    public float $midterm_requirement;
    public float $final_quiz_1;
    public float $final_quiz_2;
    public float $final_quiz_3;
    public float $final_exam;
    public float $final_requirement;

    public static function group(): string
    {
        return 'perfectscore';
    }
}
