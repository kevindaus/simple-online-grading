<?php

namespace App\Tables\Columns;

use App\Models\User;
use Filament\Tables\Columns\Column;
use Spatie\Permission\Models\Role;

class RoleColumn extends Column
{
    protected string $view = 'tables.columns.role-column';
    /**
     * @var mixed
     */
    public $user_id;
    public $roles;

    public function data()
    {
        $state = $this->getState();
        return [
            'roles' => User::find($state)->roles()->first()->name,
        ];
    }

}
