<?php

namespace App\Tables\Columns;

use App\Exceptions\IncompleteGradeScoreException;
use App\Models\Grade;
use App\Models\User;
use App\Service\GradeCalculator;
use Filament\Tables\Columns\Column;

class GradeColumn extends Column
{
    protected string $view = 'tables.columns.grade-column';
    /**
     * @var GradeCalculator
     */
    private $gradeCalculator;


    public function data()
    {
        $state = $this->getState();
        $gradeRecord = Grade::find($state);
        $this->gradeCalculator = new GradeCalculator($gradeRecord);
        $finalGrade = $this->gradeCalculator->getFinalGrade();
        return [
            'computed_grade' => sprintf("%s",$this->gradeCalculator->gradeViaPointSystem()),
        ];

    }


}
