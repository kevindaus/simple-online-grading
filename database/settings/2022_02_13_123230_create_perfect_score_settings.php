<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class CreatePerfectScoreSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add("perfectscore.prelim_quiz_1",50);
        $this->migrator->add("perfectscore.prelim_quiz_2",30);
        $this->migrator->add("perfectscore.prelim_quiz_3",25);
        $this->migrator->add("perfectscore.prelim_exam",100);
        $this->migrator->add("perfectscore.prelim_requirement",95);
        $this->migrator->add("perfectscore.midterm_quiz_1",50);
        $this->migrator->add("perfectscore.midterm_quiz_2",50);
        $this->migrator->add("perfectscore.midterm_quiz_3",50);
        $this->migrator->add("perfectscore.midterm_exam",100);
        $this->migrator->add("perfectscore.midterm_requirement",150);
        $this->migrator->add("perfectscore.final_quiz_1",20);
        $this->migrator->add("perfectscore.final_quiz_2",100);
        $this->migrator->add("perfectscore.final_quiz_3",100);
        $this->migrator->add("perfectscore.final_exam",100);
        $this->migrator->add("perfectscore.final_requirement",150);
    }
}
