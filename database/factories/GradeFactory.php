<?php

namespace Database\Factories;

use App\Models\Grade;
use App\Models\Subject;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;
use phpDocumentor\Reflection\Types\Collection;

class GradeFactory extends Factory
{
    protected $model = Grade::class;

    public function definition(): array
    {
        return [
            'semester' => collect([Grade::FIRST_SEMESTER, Grade::SECOND_SEMESTER])->random(),
            'prelim_quiz_1' => $this->faker->numberBetween(70,100),
            'prelim_quiz_2' => $this->faker->numberBetween(70,100),
            'prelim_quiz_3' => $this->faker->numberBetween(70,100),
            'prelim_requirement' => $this->faker->numberBetween(70,100),
            'prelim_exam' => $this->faker->numberBetween(70,100),
            'midterm_quiz_1' => $this->faker->numberBetween(70,100),
            'midterm_quiz_2' => $this->faker->numberBetween(70,100),
            'midterm_quiz_3' => $this->faker->numberBetween(70,100),
            'midterm_requirement' => $this->faker->numberBetween(70,100),
            'midterm_exam' => $this->faker->numberBetween(70,100),
            'final_quiz_1' => $this->faker->numberBetween(70,100),
            'final_quiz_2' => $this->faker->numberBetween(70,100),
            'final_quiz_3' => $this->faker->numberBetween(70,100),
            'final_requirement' => $this->faker->numberBetween(70,100),
            'final_exam' => $this->faker->numberBetween(70,100),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'status' => $this->faker->word,

            'user_id' => function () {
                return User::factory()->create()->id;
            },
            'subject_id' => function () {
                return Subject::factory()->create()->id;
            },
        ];
    }
}
