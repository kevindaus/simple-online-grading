<?php

namespace Database\Seeders;

use App\Models\Grade;
use Illuminate\Database\Seeder;

class ExamsSeeder extends Seeder
{
    public function run()
    {
        $allGrades = Grade::all();
        foreach ($allGrades as $currentGrade) {
            /* @var Grade $currentGrade */
            $currentGrade->prelim_exams = [
                "Exam 1" => $currentGrade->prelim_exam,
            ];
            $currentGrade->midterm_exams = [
                "Exam 1" => $currentGrade->midterm_exam,
            ];
            $currentGrade->finals_exams = [
                "Exam 1" => $currentGrade->final_exam,
            ];

            $currentGrade->save();
        }
    }
}
