<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class SuperAdminSeeder extends Seeder
{
    public function run()
    {
        /* @var $adminUser User*/
        $adminUser = User::factory()->create([
            'identification_number'=>'02-0001',
            'name'=>'Admin',
            'email'=>'admin@admin.com',
            'password'=>\Hash::make('password'),
        ]);
        $adminRole = Role::findOrCreate(User::ADMINISTRATOR_ROLE);
        $adminUser->assignRole($adminRole);
    }
}
