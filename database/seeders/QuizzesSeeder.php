<?php

namespace Database\Seeders;

use App\Models\Grade;
use Illuminate\Database\Seeder;

class QuizzesSeeder extends Seeder
{
    public function run()
    {
        $allGrades = Grade::all();
        foreach ($allGrades as $currentGrade) {
            /* @var Grade $currentGrade */
            $currentGrade->prelim_quizzes = [
                "Quiz 1" => $currentGrade->prelim_quiz_1,
                "Quiz 2" => $currentGrade->prelim_quiz_2,
                "Quiz 3" => $currentGrade->prelim_quiz_3,
            ];
            $currentGrade->midterm_quizzes = [
                "Quiz 1" => $currentGrade->midterm_quiz_1,
                "Quiz 2" => $currentGrade->midterm_quiz_2,
                "Quiz 3" => $currentGrade->midterm_quiz_3,
            ];
            $currentGrade->finals_quizzes = [
                "Quiz 1" => $currentGrade->final_quiz_1,
                "Quiz 2" => $currentGrade->final_quiz_2,
                "Quiz 3" => $currentGrade->final_quiz_3,
            ];

            $currentGrade->save();
        }
    }
}
