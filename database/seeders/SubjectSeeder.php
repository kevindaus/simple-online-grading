<?php

namespace Database\Seeders;

use App\Models\Grade;
use App\Models\Subject;
use App\Models\User;
use Illuminate\Database\Seeder;

class SubjectSeeder extends Seeder
{
    public function run()
    {
        $defaultSubject = [
            [
                "id" => 1,
                "name" => "GE ARTS",
                "description" => "Art Appreciation",
                'year_level' => User::FIRST_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 2,
                "name" => "CWTS 1",
                "description" => "Civic Welfare Training Service 1",
                'year_level' => User::FIRST_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 3,
                "name" => "IT PC 2",
                "description" => "Computer Programming 1",
                'year_level' => User::FIRST_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 4,
                "name" => "IT PC 1",
                "description" => "Introduction to Computing",
                'year_level' => User::FIRST_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 5,
                "name" => "GE FIL 1",
                "description" => "Komuniskasyon sa Akademikong Filipino",
                'year_level' => User::FIRST_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 6,
                "name" => "GE MATH",
                "description" => "Mathematics in Modern World",
                'year_level' => User::FIRST_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 7,
                "name" => "PATHF 1",
                "description" => "Physical Activities Toward Health and Fitness 1",
                'year_level' => User::FIRST_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 8,
                "name" => "IT ECC 4",
                "description" => "Data Structure and Algorithm",
                'year_level' => User::FIRST_YEAR_LEVEL,
                'semester' => Subject::SECOND_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 9,
                "name" => "GE WORLD",
                "description" => "The Contemporary World",
                'year_level' => User::FIRST_YEAR_LEVEL,
                'semester' => Subject::SECOND_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 10,
                "name" => "IT PC 1",
                "description" => "Discrete Mathematics",
                'year_level' => User::FIRST_YEAR_LEVEL,
                'semester' => Subject::SECOND_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 11,
                "name" => "CWTS 2",
                "description" => "Civic Welfare Training Service 2",
                'year_level' => User::FIRST_YEAR_LEVEL,
                'semester' => Subject::SECOND_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 12,
                "name" => "GE FIL 2",
                "description" => "Pagbasa at Pagsulat Tungo sa Pananaliksik",
                'year_level' => User::FIRST_YEAR_LEVEL,
                'semester' => Subject::SECOND_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 13,
                "name" => "PATHF 2",
                "description" => "Physical Activities Toward Health and Fitness 2",
                'year_level' => User::FIRST_YEAR_LEVEL,
                'semester' => Subject::SECOND_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 14,
                "name" => "IT ECC 3",
                "description" => "Computer Programming 2",
                'year_level' => User::FIRST_YEAR_LEVEL,
                'semester' => Subject::SECOND_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 15,
                "name" => "GE STS",
                "description" => "Science, Technology, and Society",
                'year_level' => User::FIRST_YEAR_LEVEL,
                'semester' => Subject::SECOND_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 17,
                "name" => "GE ETHICS",
                "description" => "Ethics",
                'year_level' => User::SECOND_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 18,
                "name" => "GE HISTORY",
                "description" => "Reading in Philippine History",
                'year_level' => User::SECOND_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 19,
                "name" => "IT ECC 5",
                "description" => "Information Management",
                'year_level' => User::SECOND_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 20,
                "name" => "IT PEC 1",
                "description" => "Object Oriented Programming",
                'year_level' => User::SECOND_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 21,
                "name" => "GE LIT",
                "description" => "Philippine Literature",
                'year_level' => User::SECOND_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 22,
                "name" => "PATHF 3",
                "description" => "Physical Activities Toward Health and Fitness 3",
                'year_level' => User::SECOND_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 23,
                "name" => "IT PC 6",
                "description" => "Networking 1",
                'year_level' => User::SECOND_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 24,
                "name" => "IT PEC 2",
                "description" => "Platform Technologies",
                'year_level' => User::SECOND_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 25,
                "name" => "IT PC 2",
                "description" => "Integrative Programming and Technology",
                'year_level' => User::SECOND_YEAR_LEVEL,
                'semester' => Subject::SECOND_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 26,
                "name" => "GE MS RIZAL",
                "description" => "Life and Work of Rizal",
                'year_level' => User::SECOND_YEAR_LEVEL,
                'semester' => Subject::SECOND_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 27,
                "name" => "IT PC 7",
                "description" => "Networking 2",
                'year_level' => User::SECOND_YEAR_LEVEL,
                'semester' => Subject::SECOND_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 28,
                "name" => "STAT 1",
                "description" => "Statistics",
                'year_level' => User::SECOND_YEAR_LEVEL,
                'semester' => Subject::SECOND_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 29,
                "name" => "GE COMM",
                "description" => "Purposive Communication",
                'year_level' => User::SECOND_YEAR_LEVEL,
                'semester' => Subject::SECOND_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 30,
                "name" => "IT PC 3",
                "description" => "Systems Integration and Architecture 1",
                'year_level' => User::SECOND_YEAR_LEVEL,
                'semester' => Subject::SECOND_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 31,
                "name" => "PATHF 4",
                "description" => "Physical Activities Towards Health and Fitness",
                'year_level' => User::SECOND_YEAR_LEVEL,
                'semester' => Subject::SECOND_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 32,
                "name" => "ITT2 1 ",
                "description" => "Ethical Hacking",
                'year_level' => User::THIRD_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 33,
                "name" => "ITT2 2 ",
                "description" => "Networking Programming",
                'year_level' => User::THIRD_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 34,
                "name" => "IT PEC 3 ",
                "description" => "Integrative Programming and Technologies 2",
                'year_level' => User::THIRD_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 35,
                "name" => "IT PEC 4 ",
                "description" => "Systems Integration and Architecture 2",
                'year_level' => User::THIRD_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 36,
                "name" => "IT PC 4 ",
                "description" => "Software Engineering",
                'year_level' => User::THIRD_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 37,
                "name" => "ITT2_3 ",
                "description" => "Network Design and Management",
                'year_level' => User::THIRD_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 38,
                "name" => "IT PC 5",
                "description" => "Quantitative Methods (Including Modeling & Simulation)",
                'year_level' => User::THIRD_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
//            [
//                "id" => 39,
//                "name" => "A302   ITT2 1 ",
//                "description" => "Ethical Hacking",
//                'year_level' => User::THIRD_YEAR_LEVEL,
//                'semester' => Subject::FIRST_SEMESTER,
//                "created_at" => now(),
//                "updated_at" => now()
//            ],
//            [
//                "id" => 40,
//                "name" => "A303   ITT2 2 ",
//                "description" => "Networking Programming",
//                "created_at" => now(),
//                "updated_at" => now()
//            ],
//            [
//                "id" => 41,
//                "name" => "A304   IT PEC 3 ",
//                "description" => "Integrative Programming and Technologies 2",
//                "created_at" => now(),
//                "updated_at" => now()
//            ],
//            [
//                "id" => 42,
//                "name" => "A305   IT PEC 4 ",
//                "description" => "Systems Integration and Architecture 2",
//                "created_at" => now(),
//                "updated_at" => now()
//            ],
//            [
//                "id" => 43,
//                "name" => "A306   IT PC 4 ",
//                "description" => "Software Engineering",
//                "created_at" => now(),
//                "updated_at" => now()
//            ],
//            [
//                "id" => 44,
//                "name" => "A307   ITT2 3 ",
//                "description" => "Network Design and Management",
//                "created_at" => now(),
//                "updated_at" => now()
//            ],
//            [
//                "id" => 45,
//                "name" => "A308   IT PC 5",
//                "description" => "Quantitative Methods (Including Modeling & Simulation)",
//                "created_at" => now(),
//                "updated_at" => now()
//            ],
            [
                "id" => 46,
                "name" => "T PC 11",
                "description" => "Systems Administration and Maintenance",
                'year_level' => User::FOURTH_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 47,
                "name" => "IT PC 12",
                "description" => "Social Issues and Professional Practice",
                'year_level' => User::FOURTH_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 48,
                "name" => "ITT2 6",
                "description" => "Network Security and Services",
                'year_level' => User::FOURTH_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 49,
                "name" => "IT PC 10",
                "description" => "Information Assurance and Security 2",
                'year_level' => User::FOURTH_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 50,
                "name" => "IT PC 100B",
                "description" => "Capstone Project and Research 2",
                'year_level' => User::FOURTH_YEAR_LEVEL,
                'semester' => Subject::FIRST_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 51,
                "name" => "IT PC 99",
                "description" => "Practicum (486 Hours)",
                'year_level' => User::FOURTH_YEAR_LEVEL,
                'semester' => Subject::SECOND_SEMESTER,
                "created_at" => now(),
                "updated_at" => now()
            ]
        ];
        foreach ($defaultSubject as $currentSubject) {
            Subject::create($currentSubject);
        }


    }
}
