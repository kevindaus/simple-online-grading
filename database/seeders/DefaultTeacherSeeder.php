<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class DefaultTeacherSeeder extends Seeder
{
    public function run()
    {
        $defaultTeacher = [
            [
                "id" => 41,
                "identification_number" => "02-0023",
                "name" => "Butac, Shirley",
                "mobile_number" => null,
                "year_level" => User::FIRST_YEAR_LEVEL,
                "course" => null,
                "email" => null,
                "email_verified_at" => null,
                "password" => "$2y$10$/H2FyqaNDTvP1H.WPPBceu3QJjOe7zFOlXGoqru9P/Bkfm1xveaIG",
                "remember_token" => null,
                "profile_photo_path" => null,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 42,
                "identification_number" => "02-0024",
                "name" => "Thiam, John",
                "mobile_number" => null,
                "year_level" => User::SECOND_YEAR_LEVEL,
                "course" => null,
                "email" => null,
                "email_verified_at" => null,
                "password" => "$2y$10$/H2FyqaNDTvP1H.WPPBceu3QJjOe7zFOlXGoqru9P/Bkfm1xveaIG",
                "remember_token" => null,
                "profile_photo_path" => null,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 43,
                "identification_number" => "02-0025",
                "name" => "Acosta, Achristine",
                "mobile_number" => null,
                "year_level" => User::THIRD_YEAR_LEVEL,
                "course" => null,
                "email" => null,
                "email_verified_at" => null,
                "password" => "$2y$10$/H2FyqaNDTvP1H.WPPBceu3QJjOe7zFOlXGoqru9P/Bkfm1xveaIG",
                "remember_token" => null,
                "profile_photo_path" => null,
                "created_at" => now(),
                "updated_at" => now()
            ],
            [
                "id" => 44,
                "identification_number" => "02-0026",
                "name" => "Yattar, Philip",
                "mobile_number" => null,
                "year_level" => User::FOURTH_YEAR_LEVEL,
                "course" => null,
                "email" => null,
                "email_verified_at" => null,
                "password" => "$2y$10$/H2FyqaNDTvP1H.WPPBceu3QJjOe7zFOlXGoqru9P/Bkfm1xveaIG",
                "remember_token" => null,
                "profile_photo_path" => null,
                "created_at" => now(),
                "updated_at" => now()
            ],
//            [
//                "id" => 45,
//                "identification_number" => "02-0027",
//                "name" => "Valdez, Jane",
//                "mobile_number" => null,
//                "year_level" => null,
//                "course" => null,
//                "email" => null,
//                "email_verified_at" => null,
//                "password" => "$2y$10$/H2FyqaNDTvP1H.WPPBceu3QJjOe7zFOlXGoqru9P/Bkfm1xveaIG",
//                "remember_token" => null,
//                "profile_photo_path" => null,
//                "created_at" => now(),
//                "updated_at" => now()
//            ],
        ];

        $teacherRole = Role::findOrCreate(User::TEACHER_ROLE);
        foreach ($defaultTeacher as $currentTeacher) {
            $newTeacher = User::create($currentTeacher);
            $newTeacher->assignRole($teacherRole);
        }
    }
}
