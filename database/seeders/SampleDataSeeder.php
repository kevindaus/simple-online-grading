<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class SampleDataSeeder extends Seeder
{
    public function run()
    {
        $adminRole = Role::findOrCreate(User::ADMINISTRATOR_ROLE);
        $teacherRole = Role::findOrCreate(User::TEACHER_ROLE);
        $studentRole = Role::findOrCreate(User::STUDENT_ROLE);

        $adminUser = User::factory()->create([
            'identification_number'=>'111111',
            'name'=>'admin user',
            'email'=>'admin_user@gmail.com',
            'password'=>\Hash::make('password'),
        ]);
        $adminRole = Role::findOrCreate(User::ADMINISTRATOR_ROLE);
        $adminUser->assignRole($adminRole);

        $teacherUser = User::factory()->create([
            'identification_number'=>'222222',
            'name'=>'teacher user',
            'email'=>'teacher_user@gmail.com',
            'password'=>\Hash::make('password'),
        ]);
        $teacherUser->assignRole($teacherRole);

        $studentUser = User::factory()->create([
            'identification_number'=>'333333',
            'name'=>'student user',
            'email'=>'student_user@gmail.com',
            'password'=>\Hash::make('password'),
        ]);
        $studentUser->assignRole($studentRole);


    }
}
