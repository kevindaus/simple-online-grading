<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class DefaultRoleSeeder extends Seeder
{
    public function run()
    {
        $adminRole = Role::findOrCreate(User::ADMINISTRATOR_ROLE);
        $teacherRole = Role::findOrCreate(User::TEACHER_ROLE);
        $studentRole = Role::findOrCreate(User::STUDENT_ROLE);
    }
}
