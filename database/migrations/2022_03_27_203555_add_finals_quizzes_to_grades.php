<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFinalsQuizzesToGrades extends Migration
{
    public function up()
    {
        Schema::table('grades', function (Blueprint $table) {
            $table->json('finals_quizzes')->nullable(true);
        });
    }

    public function down()
    {
        Schema::table('grades', function (Blueprint $table) {
            $table->dropColumn('finals_quizzes');
        });
    }
}
