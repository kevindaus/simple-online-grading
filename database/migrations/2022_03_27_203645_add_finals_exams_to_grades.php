<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFinalsExamsToGrades extends Migration
{
    public function up()
    {
        Schema::table('grades', function (Blueprint $table) {
            $table->json('finals_exams')->nullable(true);
        });
    }

    public function down()
    {
        Schema::table('grades', function (Blueprint $table) {
            $table->dropColumn('finals_exams');
        });
    }
}
