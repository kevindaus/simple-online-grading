<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPrelimExamsToGrades extends Migration
{
    public function up()
    {
        Schema::table('grades', function (Blueprint $table) {
            $table->json('prelim_exams')->nullable(true);
        });
    }

    public function down()
    {
        Schema::table('grades', function (Blueprint $table) {
            $table->dropColumn('prelim_exams');
        });
    }
}
