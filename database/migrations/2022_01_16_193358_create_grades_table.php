<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGradesTable extends Migration
{
    public function up()
    {
        Schema::create('grades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('user_id')->nullable()->constrained('users')->onDelete('cascade');
            $table->foreignId('subject_id')->nullable()->constrained('subjects')->onDelete('cascade');
            $table->string("semester")->nullable();
            $table->string("year_level")->nullable();
            $table->float("prelim_quiz_1")->nullable();
            $table->float("prelim_quiz_2")->nullable();
            $table->float("prelim_quiz_3")->nullable();
            $table->float("prelim_requirement")->nullable();
            $table->float("prelim_exam")->nullable();
            $table->float("midterm_quiz_1")->nullable();
            $table->float("midterm_quiz_2")->nullable();
            $table->float("midterm_quiz_3")->nullable();
            $table->float("midterm_requirement")->nullable();
            $table->float("midterm_exam")->nullable();
            $table->float("final_quiz_1")->nullable();
            $table->float("final_quiz_2")->nullable();
            $table->float("final_quiz_3")->nullable();
            $table->float("final_requirement")->nullable();
            $table->float("final_exam")->nullable();

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('grades');
    }
}
