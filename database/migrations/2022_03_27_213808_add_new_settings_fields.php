<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewSettingsFields extends \Spatie\LaravelSettings\Migrations\SettingsMigration
{
    public function up()
    {
        $this->migrator->add("perfectscore.prelim_quizzes",[
            "Quiz 1"=> 50,
            "Quiz 2"=> 30,
            "Quiz 3"=> 25,
        ]);
        $this->migrator->add("perfectscore.prelim_exams",[
            "Exam 1"=>100,
        ]);
        $this->migrator->add("perfectscore.midterm_quizzes",[
            "Quiz 1"=> 50,
            "Quiz 2"=> 50,
            "Quiz 3"=> 50,
        ]);
        $this->migrator->add("perfectscore.midterm_exams",[
            "Exam 1"=>100,
        ]);
        $this->migrator->add("perfectscore.finals_quizzes",[
            "Quiz 1"=> 20,
            "Quiz 2"=> 100,
            "Quiz 3"=> 100
        ]);
        $this->migrator->add("perfectscore.finals_exams",[
            "Exam 1"=>100,
        ]);
    }

    public function down()
    {

    }
}
